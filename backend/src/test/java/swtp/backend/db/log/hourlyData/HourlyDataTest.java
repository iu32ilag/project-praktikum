package swtp.backend.db.log.hourlyData;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swtp.backend.db.devicesData.DevicesDataId;

import static org.junit.jupiter.api.Assertions.*;

class HourlyDataTest {

    int gid = 54;
    double time = 6.4;

    double value = 63.9;
    double total = 67.6;
    double peak = 680.2;
    double lowest = 0.39;
    double idleTime = 290;
    HourlyData hourlyData = new HourlyData();
   HourlyDataId dev=new HourlyDataId();
    HourlyDataId dev2=new HourlyDataId(2,2.5);
    @BeforeEach
    void setup() {
        hourlyData.setGid(gid);
        hourlyData.setTime(time);
        hourlyData.setValue(value);
        hourlyData.setTotal(total);
        hourlyData.setPeak(peak);
        hourlyData.setLowest(lowest);
        hourlyData.setIdleTime(idleTime);
        hourlyData = new HourlyData(gid,time,value,total,peak,lowest,idleTime);
    }
    @Test
    void testToString() {
        String expected = "HourlyData{" +
                "gid=" + gid +
                ", time=" + time +
                ", value=" + value +
                ", total=" + total +
                ", peak=" + peak +
                ", lowest=" + lowest +
                ", idleTime=" + idleTime +
                '}';
        assertEquals(expected,hourlyData.toString());
    }

    @Test
    void getGid() {
        assertEquals(gid,hourlyData.getGid());

    }

    @Test
    void getTime() {
        assertEquals(time,hourlyData.getTime());
    }

    @Test
    void getValue() {
        assertEquals(value,hourlyData.getValue());
    }

    @Test
    void getTotal() {
        assertEquals(total,hourlyData.getTotal());
    }

    @Test
    void getPeak() {
        assertEquals(peak,hourlyData.getPeak());
    }

    @Test
    void getLowest() {
        assertEquals(lowest,hourlyData.getLowest());
    }

    @Test
    void getIdleTime() {
        assertEquals(idleTime,hourlyData.getIdleTime());
    }
}