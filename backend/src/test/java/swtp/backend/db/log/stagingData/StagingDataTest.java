package swtp.backend.db.log.stagingData;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swtp.backend.db.devicesData.DevicesDataId;
import swtp.backend.db.log.hourlyData.HourlyData;

import static org.junit.jupiter.api.Assertions.*;

class StagingDataTest {
    double value = 6.2;
    double idletime = 2.0;
    double time = 44.2;
    int gid = 5;
    StagingData stagingData = new StagingData();
    StagingDataId dev=new StagingDataId();
    StagingDataId dev2=new StagingDataId(3, 2.2);
    @BeforeEach
    void setup() {
        stagingData = new StagingData(gid,time,value);
        stagingData.setGid(gid);
        stagingData.setTime(time);
        stagingData.setValue(value);
        stagingData.setIdleTime(idletime);


    }


    @Test
    void getValue() {
        assertEquals(value,stagingData.getValue());
    }

    @Test
    void getIdleTime() {
        assertEquals(idletime,stagingData.getIdleTime());
    }

    @Test
    void testToString() {
        String expected = "StagingData [gid=" + gid + ", time=" + time + ", value=" + value + "]";
        assertEquals(expected,stagingData.toString());
    }


    @Test
    void getGid() {
        assertEquals(gid,stagingData.getGid());
    }

    @Test
    void getTime() {
        assertEquals(time,stagingData.getTime());
    }
}