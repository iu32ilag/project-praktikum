package swtp.backend.db.log.weeklyData;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swtp.backend.db.devicesData.DevicesDataId;
import swtp.backend.db.log.dailyData.DailyData;

import static org.junit.jupiter.api.Assertions.*;

class WeeklyDataTest {
        int gid = 2;
        double time = 4.2;

        double value = 4.2;
        double total = 90.2;
        double peak = 120.2;
        double lowest = 1.2;
        double idleTime = 64.1;
        WeeklyData weeklydata = new WeeklyData();
    WeeklyDataId dev=new WeeklyDataId();
    WeeklyDataId dev2=new WeeklyDataId(3, 2.2);


        @BeforeEach
        void setup() {
            weeklydata.setGid(gid);
            weeklydata.setTime(time);
            weeklydata.setValue(value);
            weeklydata.setTotal(total);
            weeklydata.setPeak(peak);
            weeklydata.setLowest(lowest);
            weeklydata.setIdleTime(idleTime);
            weeklydata = new WeeklyData(gid,time,value,total,peak,lowest,idleTime);
        }

        @Test
        void testToString() {
            String expected = "WeeklyData{" +
                    "gid=" + gid +
                    ", time=" + time +
                    ", value=" + value +
                    ", total=" + total +
                    ", peak=" + peak +
                    ", lowest=" + lowest +
                    ", idleTime=" + idleTime +
                    '}';
            assertEquals(expected, weeklydata.toString());
        }

        @Test
        void getGid() {
            assertEquals(gid, weeklydata.getGid());
        }

        @Test
        void getTime() {
            assertEquals(time, weeklydata.getTime());
        }

        @Test
        void getValue() {
            assertEquals(value, weeklydata.getValue());
        }

        @Test
        void getTotal() {
            assertEquals(total, weeklydata.getTotal());
        }

        @Test
        void getPeak() {
            assertEquals(peak, weeklydata.getPeak());
        }

        @Test
        void getLowest() {
            assertEquals(lowest, weeklydata.getLowest());
        }

        @Test
        void getIdleTime() {
            assertEquals(idleTime, weeklydata.getIdleTime());
        }
    }