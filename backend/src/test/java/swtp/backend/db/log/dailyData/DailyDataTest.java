package swtp.backend.db.log.dailyData;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swtp.backend.db.devicesData.DevicesDataId;

import static org.junit.jupiter.api.Assertions.*;

class DailyDataTest {

     int gid = 84;
    double time = 6.2;

     double value = 43.9;
     double total = 65.6;
     double peak = 620.2;
     double lowest = 0.35;
     double idleTime = 460;
     DailyData dailyData = new DailyData();
    DailyDataId dev=new DailyDataId();
    DailyDataId dev2=new DailyDataId(3, 2.2);

     @BeforeEach
     void setup() {
         dailyData.setGid(gid);
         dailyData.setTime(time);
         dailyData.setValue(value);
         dailyData.setTotal(total);
         dailyData.setPeak(peak);
         dailyData.setLowest(lowest);
         dailyData.setIdleTime(idleTime);
         dailyData = new DailyData(gid,time,value,total,peak,lowest,idleTime);
     }

         @Test
    void testToString() {
        String expected = "DailyData{" +
                "gid=" + gid +
                ", time=" + time +
                ", value=" + value +
                ", total=" + total +
                ", peak=" + peak +
                ", lowest=" + lowest +
                ", idleTime=" + idleTime +
                '}';
        assertEquals(expected,dailyData.toString());
    }

    @Test
    void getGid() {
         assertEquals(gid,dailyData.getGid());
    }

    @Test
    void getTime() {
         assertEquals(time,dailyData.getTime());
    }

    @Test
    void getValue() {
         assertEquals(value,dailyData.getValue());
    }

    @Test
    void getTotal() {
         assertEquals(total,dailyData.getTotal());
    }

    @Test
    void getPeak() {
         assertEquals(peak,dailyData.getPeak());
    }

    @Test
    void getLowest() {
         assertEquals(lowest,dailyData.getLowest());
    }

    @Test
    void getIdleTime() {
         assertEquals(idleTime,dailyData.getIdleTime());
    }
}