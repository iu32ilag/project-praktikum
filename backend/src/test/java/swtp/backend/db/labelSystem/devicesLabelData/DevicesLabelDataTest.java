package swtp.backend.db.labelSystem.devicesLabelData;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swtp.backend.db.devicesData.DevicesDataId;

import static org.junit.jupiter.api.Assertions.*;

class DevicesLabelDataTest {
    int gid = 69;
    int lid = 69;
    DevicesLabelData device = new DevicesLabelData();
    DevicesLabelDataId dev=new DevicesLabelDataId();
    DevicesLabelDataId dev2=new DevicesLabelDataId(3, 2);
    @BeforeEach
    void setup(){
        device.setGid(gid);
        device.setLid(lid);
        device = new DevicesLabelData(gid, lid);
    }


    @Test
    void testToString() {
        String expected= "LabelsCatData [gid=" + gid + ", lid=" + lid + "]";
        assertEquals(expected,device.toString());
    }

    @Test
    void getGid() {
        assertEquals(gid,device.getGid());
    }

    @Test
    void getLid() {
        assertEquals(lid,device.getLid());
    }
}