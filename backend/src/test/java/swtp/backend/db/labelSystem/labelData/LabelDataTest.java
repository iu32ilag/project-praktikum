package swtp.backend.db.labelSystem.labelData;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swtp.backend.db.devicesData.DevicesData;
import swtp.backend.db.labelSystem.devicesLabelData.DevicesLabelDataId;

import static org.junit.jupiter.api.Assertions.*;

class LabelDataTest {
    int lid = 880;
    String name = "Gaz";
    String description = "Hey hey";
    boolean usercr = true;
    LabelData d = new LabelData();
   LabelDataId dev=new LabelDataId();
    LabelDataId dev2=new LabelDataId(3, 2);
    @BeforeEach

    void setup() {
        d.setLid(lid);
        d.setDescription(description);
        d.setName(name);
        d.setUserCreated(usercr);
        d = new LabelData(lid,name,description,usercr);
    }



    @Test
    void testToString() {
        String expected = "LabelData [lid=" + lid +
                ", name=" + name + ", " +
                "description=" + description + ", " +
                "userCreated=" + usercr +
                "]";
        assertEquals(expected,d.toString());



    }

    @Test
    void getLid() {
        assertEquals(lid,d.getLid());
    }

    @Test
    void getName() {
        assertEquals(name,d.getName());
    }

    @Test
    void getDescription() {
        assertEquals(description,d.getDescription());
    }

    @Test
    void isUserCreated() {
        assertEquals(usercr,d.isUserCreated());
    }
}
