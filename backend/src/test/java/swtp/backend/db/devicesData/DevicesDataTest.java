package swtp.backend.db.devicesData;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swtp.backend.db.labelSystem.devicesLabelData.DevicesLabelData;
import swtp.backend.db.labelSystem.devicesLabelData.DevicesLabelDataId;
import swtp.backend.db.labelSystem.labelData.LabelData;
import swtp.backend.db.labelSystem.labelData.LabelDataId;
import swtp.backend.db.log.dailyData.DailyData;
import swtp.backend.db.log.dailyData.DailyDataId;
import swtp.backend.db.log.hourlyData.HourlyData;
import swtp.backend.db.log.hourlyData.HourlyDataId;
import swtp.backend.db.log.stagingData.StagingDataId;
import swtp.backend.db.log.weeklyData.WeeklyData;
import swtp.backend.db.log.weeklyData.WeeklyDataId;
import swtp.backend.db.sequenz.SequenzData;
import swtp.backend.utils.SequenzValue;
import swtp.backend.utils.TimeData;

import static org.junit.jupiter.api.Assertions.*;
class DevicesDataTest {

    DevicesData devicesData;
    int gid = 5;
    boolean active= true;
    double Average = 7;
    boolean idle=true;
    double Alltime=9;
    String CustomId= "d";
    double Firstseen=3;
    double lastseen =2;
    double lastValue=3;
    double idleTime=4;
    double lowest=5;
    double Peak=7;

    DevicesDataId dev=new DevicesDataId();
    DevicesDataId dev2=new DevicesDataId(3, 2.2);


    @BeforeEach

    void setup() {
        devicesData=new DevicesData();
        devicesData.setGid(gid);
        devicesData.setActive(active);
        devicesData.setAverage(Average);
        devicesData.setIdle(idle);
        devicesData.setAllTime(Alltime);
        devicesData.setCustomID(CustomId);
        devicesData.setFirstSeen(Firstseen);
        devicesData.setLastSeen(lastseen);
        devicesData.setLastValue(lastValue);
        devicesData.setIdleTime(idleTime);
        devicesData.setLowest(lowest);
        devicesData.setPeak(Peak);

        devicesData=new DevicesData(gid,CustomId,active,Firstseen,lastseen,idleTime,Peak,Average,lowest,Alltime,lastValue,idle);


    }

    @Test
    void returnDevice() {
        assertNotNull(devicesData);
    }
    @Test
    void getid(){
        assertEquals(gid,devicesData.getGid());


    }
    @Test
    void Istactive() {
        assertEquals(active, devicesData.isActive());
    }
    @Test
    void getaverag(){
        assertEquals(Average,devicesData.getAverage());}
    @Test
    void istIdle (){
        assertEquals(idle,devicesData.isIdle());}
    @Test
    void getAlltime(){
        assertEquals(Alltime,devicesData.getAllTime());}
    @Test
    void getCustomId(){
        assertEquals(CustomId,devicesData.getCustomID());}
    @Test
    void getFirstseen(){
        assertEquals(Firstseen,devicesData.getFirstSeen());}
    @Test
    void getlastseen(){
        assertEquals(lastseen,devicesData.getLastSeen());}
    @Test
    void getlastValue(){
        assertEquals(lastValue,devicesData.getLastValue());}
    @Test
    void getidleTime(){
        assertEquals(idleTime,devicesData.getIdleTime());}
    @Test
    void getAverage(){
        assertEquals(Average,devicesData.getAverage());}
    @Test
    void getlowest(){
        assertEquals(lowest,devicesData.getLowest());}
    @Test
    void getPeak(){
        assertEquals(Peak,devicesData.getPeak());}
    @Test
    void testtoString (){
        assertEquals("DailyData{" +
                "gid=" + gid +
                ", customID=" + CustomId +
                ", active=" + active +
                ", firstSeen=" + Firstseen +
                ", lastSeen=" + lastseen +
                ", idleTime=" + idleTime +
                ", peak=" + Peak +
                ", average=" + Average +
                ", lowest=" + lowest +
                ", allTime=" + Alltime +
                ", lastValue=" + lastValue +
                ", isIdle=" + idle +
                '}',devicesData.toString()
        );
    }

    @Test
    void equalstest(){
        EqualsVerifier.forClass(DevicesDataId.class).suppress(Warning.NONFINAL_FIELDS).suppress(Warning.STRICT_INHERITANCE).verify();
        EqualsVerifier.forClass(DevicesLabelDataId.class).suppress(Warning.NONFINAL_FIELDS).suppress(Warning.STRICT_INHERITANCE).verify();
        EqualsVerifier.forClass(LabelDataId.class).suppress(Warning.NONFINAL_FIELDS).suppress(Warning.STRICT_INHERITANCE).verify();
        EqualsVerifier.forClass(DailyDataId.class).suppress(Warning.NONFINAL_FIELDS).suppress(Warning.STRICT_INHERITANCE).verify();
        EqualsVerifier.forClass(HourlyDataId.class).suppress(Warning.NONFINAL_FIELDS).suppress(Warning.STRICT_INHERITANCE).verify();
        EqualsVerifier.forClass(StagingDataId.class).suppress(Warning.NONFINAL_FIELDS).suppress(Warning.STRICT_INHERITANCE).verify();
        EqualsVerifier.forClass(WeeklyDataId.class).suppress(Warning.NONFINAL_FIELDS).suppress(Warning.STRICT_INHERITANCE).verify();
        DevicesData devicesData1 = DevicesData.builder().gid(gid).customID(CustomId).active(active).firstSeen(Firstseen).lastSeen(lastseen).idleTime(idleTime).peak(Peak).average(Average).lowest(lowest).allTime(Alltime).lastValue(lastValue).isIdle(idle).build();
        assertNotNull(devicesData1);
        DevicesLabelData devicesLabelData = DevicesLabelData.builder().gid(gid).lid(456).build();
        assertNotNull(devicesLabelData);
        LabelData labelData = LabelData.builder().name("abc").lid(456).description("ewwe").userCreated(true).build();
        assertNotNull(labelData);
        DailyData dailyData = DailyData.builder().time(120).total(15.2).value(12.1).peak(Peak).gid(gid).idleTime(idleTime).lowest(lowest).build();
        assertNotNull(dailyData);
        HourlyData hourlyData = HourlyData.builder().time(1245).total(142.2).value(22.1).peak(Peak).gid(gid).idleTime(idleTime).lowest(lowest).build();
        assertNotNull(hourlyData);
        WeeklyData weeklyData = WeeklyData.builder().time(120).total(15.2).value(12.1).peak(Peak).gid(gid).idleTime(idleTime).lowest(lowest).build();
        assertNotNull(weeklyData);
        SequenzData sequenzData = SequenzData.builder().slabel("ss").sid(2).manual(true).time(120).value(12.1).gid(gid).build();
        assertNotNull(sequenzData);

    }



}
