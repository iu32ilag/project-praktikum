package swtp.backend.db.sequenz;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;
class SequenzDataTest {
    double value = 6.2;
    boolean manual = true;
    double time = 44.2;
    int gid = 5;
    long sid =23333;
    String slabel = "Ahmad" ;
    SequenzData sequenzData = new SequenzData();
    @BeforeEach
    void setup() {
        sequenzData = new SequenzData(sid, slabel, gid, value, time, manual);
        sequenzData.setGid(gid);
        sequenzData.setSid(sid);
        sequenzData.setValue(value);
        sequenzData.setTime(time);
        sequenzData.setSlabel(slabel);
        sequenzData.setManual(manual);


    }


    @Test
    void getValue() {
        assertEquals(value,sequenzData.getValue());
    }

    @Test
    void getTime() {
        assertEquals(time,sequenzData.getTime());
    }

    @Test
    void testToString() {
        String expected = "SequenzData [sid=" + sid +
                ", slabel=" + slabel + ", " +
                "gid=" + gid + ", " +
                "value=" + value + ", " +
                "time=" + time + ", " +
                "manual=" + manual +
                "]";
        assertEquals(expected,sequenzData.toString());
    }


    @Test
    void getGid() {
        assertEquals(gid,sequenzData.getGid());
    }

    @Test
    void getSid() {
        assertEquals(sid,sequenzData.getSid());
    }
    @Test
    void getSlabel(){assertEquals(slabel,sequenzData.getSlabel());}
    @Test
    void ismanual(){
        assertEquals(manual,sequenzData.isManual());

    }



}

