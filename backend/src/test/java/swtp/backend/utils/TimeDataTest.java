package swtp.backend.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TimeDataTest {
    TimeData timeData = new TimeData() {
    };

    int gid = 32;
    double time = 6.2;
    double value= 4.3;
    double total=3.9;
    double peak =1.4;
    double lowest=2.5;
    double idletime=7.3;

    @BeforeEach
    void setup() {
        timeData.setGid(gid);
        timeData.setTime(time);
        timeData.setValue(value);
        timeData.setTotal(total);
        timeData.setPeak(peak);
        timeData.setLowest(lowest);
        timeData.setIdleTime(idletime);
    }

    @Test
    void getGid() {
        assertEquals(gid, timeData.getGid());
    }

    @Test
    void getTime() {
        assertEquals(time, timeData.getTime());
    }

    @Test
    void getValue() {
        assertEquals(value, timeData.getValue());
    }

    @Test
    void getTotal() {
        assertEquals(total, timeData.getTotal());
    }

    @Test
    void getPeak() {
        assertEquals(peak, timeData.getPeak());

    }
    @Test
    void getLowest() {
        assertEquals(lowest, timeData.getLowest());
    }
    @Test
    void getIdleTime() {
        assertEquals(idletime, timeData.getIdleTime());

    }

}