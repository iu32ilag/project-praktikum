package swtp.backend.utils.statistics;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swtp.backend.utils.TimeData;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StatisticsTimeDataServiceTest {
    List<TimeData> testData;
    StatisticsTimeDataService stds = new StatisticsTimeDataService();
    TimeData timeData = new TimeData() {
    };
    int gid = 32;
    double time = 6.2;
    double value= 4.3;
    double total=3.9;
    double peak =1.4;
    double lowest=2.5;
    double idletime=7.3;

    @BeforeEach
    void setup(){

        timeData.setGid(gid);
        timeData.setTime(time);
        timeData.setValue(value);
        timeData.setTotal(total);
        timeData.setPeak(peak);
        timeData.setLowest(lowest);
        timeData.setIdleTime(idletime);
        testData = new ArrayList<>();
        testData.add(timeData);
    }

    @Test
    void gesamtUsage() {
        assertEquals(3.9,stds.gesamtUsage(testData));
    }

    @Test
    void averageUsage() {
        assertEquals(stds.averageUsage(testData),4.3);
    }

    @Test
    void peak() {

        assertEquals(stds.peak(testData),1.4);
    }

    @Test
    void lowest() {
        assertEquals(stds.lowest(testData),2.5);
    }

    @Test
    void varianz() {
        assertEquals(0.0,stds.varianz(testData));
    }

    @Test
    void idleTime() {
        assertEquals(7.3,stds.idleTime(testData));
    }

    @Test
    void standartabweichung() {
        assertEquals(0.0,stds.standartabweichung(testData));

    }
}