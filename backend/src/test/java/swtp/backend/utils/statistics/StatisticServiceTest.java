package swtp.backend.utils.statistics;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import swtp.backend.db.log.stagingData.StagingData;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("StatisticServiceTest")
public class StatisticServiceTest {

    private static final ArrayList<StagingData> list = new ArrayList<>();

    @Mock
    private final DeviceStatisticService dss = new DeviceStatisticService();
    private final StatisticService statisticService = new StatisticService(dss);
    StatisticService statisticService2 = new StatisticService();




    @BeforeAll
    static void initAll() {
        list.add(new StagingData(1,0,10));
        list.add(new StagingData(1,0,20));
        list.add(new StagingData(1,0,0));
    }

    @Test
    @Disabled("for demonstration purposes")
    void failingTest() {
        fail("Test for StatisticServiceTest failed");
    }

    @Test
    @DisplayName("Check gesamtverbrauch")
    public void gesamtUsage() {
        assertEquals(statisticService.gesamtUsage(list), 30);
    }

    @Test
    @DisplayName("Check averageUsage")
    public void averageUsage() {
        assertEquals(statisticService.averageUsage(list), 10);
    }

    @Test
    @DisplayName("Check peak")
    public void peak() {
        assertEquals(statisticService.peak(list), 20);
    }

    @Test
    @DisplayName("Check lowest")
    public void lowest() {
        assertEquals(statisticService.lowest(list), 0);
    }

    @Test
    @DisplayName("Check varianz")
    public void varianz() {
        assertEquals(statisticService.varianz(list), 66.66666666666667);
    }

    @Test
    @DisplayName("Check standartabweichung")
    public void standartabweichung() {
        assertEquals(statisticService.standartabweichung(list), 8.16496580927726);
    }

    @Test
    @DisplayName("Check idleTime")
    public void idleTime() {
        assertEquals(statisticService.idleTime(15, list), 1);
    }
}
