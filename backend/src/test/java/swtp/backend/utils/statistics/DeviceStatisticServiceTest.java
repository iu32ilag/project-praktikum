package swtp.backend.utils.statistics;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("DeviceStatisticServiceTest")
public class DeviceStatisticServiceTest {

    @Mock
    private DeviceStatisticService deviceStatisticService = new DeviceStatisticService();

    @Test
    @DisplayName("Check idle time")
    public void testCheckIdleTime() {

       assertAll("checkIdleTime",
               () -> assertEquals(deviceStatisticService.checkIdleTime(10, 9), true),
               () -> assertEquals(deviceStatisticService.checkIdleTime(10, 11), true),
               () -> assertEquals(deviceStatisticService.checkIdleTime(10, 10), true),
               () -> assertEquals(deviceStatisticService.checkIdleTime(10, 8), true),
               () -> assertEquals(deviceStatisticService.checkIdleTime(10, 12), false)
       );
    }
}
