package swtp.backend.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SequenzValueTest {
    SequenzValue sequenzValue = new SequenzValue();


         double time = 6.2;
         double value= 4.3;


    @BeforeEach
    void setup() {

            sequenzValue.setTime(time);
            sequenzValue.setValue(value);
            }

@Test
    void getTime() {
            assertEquals(time, sequenzValue.getTime());
            }
@Test
    void getValue() {
            assertEquals(value, sequenzValue.getValue());
            }
};
