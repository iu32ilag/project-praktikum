package swtp.backend.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import swtp.backend.db.log.stagingData.StagingData;
import swtp.backend.db.log.stagingData.StagingDataService;
import swtp.backend.utils.statistics.StatisticService;

import java.util.List;

/**
 * Klasse um per GET die aktuellen unbearbeitetn Geratedaten zu erhalten (Staging Data)
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping(path ="/api/v1/staging")
public class StagingDataController {

    private final StagingDataService stagingDataService;
    private final StatisticService statisticService;

    @Autowired
    public StagingDataController(StagingDataService stagingDataService,StatisticService statisticService){
        this.stagingDataService = stagingDataService;
        this.statisticService = statisticService;
    }

    /**
     * Gibt die aktuellen unbearbeiteten Gerätedaten zurück
     *
     * @return Liste von StagingData
     */

    // achtung gefährlich, zuviele Daten führen zu vielen Objecten -> memory overload
    /*@GetMapping
    public List<StagingData> getStagingData(){
        return stagingDataService.getStagingData();
    }*/

    @GetMapping(path = "/{gid}")
    public List<StagingData> getStagingDataByGid(@RequestParam("gid") int gid){
        return stagingDataService.getStagingDataByGid(gid);
    }


    /**
     * Gibt die aktuellen unbearbeiteten Gerätedaten zurück
     * beispielanfrage via RestAPI würde so aussehen
     * http://localhost:8080/api/v1/stagingDataRep/time?startTime=1670271295&endTime=1670271299
     *
     * @param startTime Startzeitpunkt als Unix Timestamp
     * @param endTime Endzeitpunkt als Unix Timestamp
     * @return Liste von StagingData
     */
    @GetMapping(path = "/time")
    public List<StagingData> getStagingDataWithTime(@RequestParam double startTime, @RequestParam double endTime){
        return stagingDataService.getStagingDataWithTime(startTime,endTime);
    }

    @GetMapping(path = "/{gid}/time")
    public List<StagingData> getStagingDataWithTimeByGid(@PathVariable("gid") int gid, @RequestParam double startTime, @RequestParam double endTime){
        return stagingDataService.findStagingDataByTimeBetweenAndGid(startTime,endTime,gid);
    }

    /**
     * Gibt den Durchschnitt der aktuellen unbearbeiteten Gerätedaten zurück
     *
     * @return Durchschnitt der aktuellen unbearbeiteten Gerätedaten
     */
    @Deprecated
    @GetMapping(path = "/average")
    public double getAverageOfAll(){
        return statisticService.averageUsage(stagingDataService.getStagingData());
    }
}
