package swtp.backend.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import swtp.backend.db.sequenz.SequenzData;
import swtp.backend.db.sequenz.SequenzDataService;
import swtp.backend.utils.helper.SequenzIdentification;
import swtp.backend.utils.sequenzLabelData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * RestController (api) für die Seqzenzdaten.
 */


@CrossOrigin(origins = "*")
@RestController
@RequestMapping(path = "/api/v1/sequenz")
public class SequenzDataController {
    private final SequenzDataService sequenzDataService;
    private final SequenzIdentification sequenzIdentification;

    @Autowired
    public SequenzDataController(SequenzDataService sequenzDataService, SequenzIdentification sequenzIdentification) {
        this.sequenzDataService = sequenzDataService;
        this.sequenzIdentification = sequenzIdentification;
    }

    @GetMapping
    public ResponseEntity<List<SequenzData>> getSequenzData() {
        List<SequenzData> list = sequenzDataService.getSequenzData();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(path = "/gid/{gid}")
    public ResponseEntity<List<SequenzData>> getSequenzDataById(@PathVariable int gid) {
        List<SequenzData> sequenzData = sequenzDataService.getSequenzDataById(gid);
        return new ResponseEntity<>(sequenzData, HttpStatus.OK);
    }


    @GetMapping(path = "/label/{slabel}")
    public ResponseEntity<List<SequenzData>> getSequenzDataByLabel(@PathVariable String slabel) {
        List<SequenzData> sequenzData = sequenzDataService.getSequenzDataByLabel(slabel);
        return new ResponseEntity<>(sequenzData, HttpStatus.OK);
    }

    @GetMapping(path = "/{gid}/{slabel}")
    public ResponseEntity<List<SequenzData>> getSequenzDataByIdAndLabel(@PathVariable int gid, @PathVariable String slabel) {
        List<SequenzData> sequenzData = sequenzDataService.getSequenzDataByIdAndLabel(gid, slabel);
        return new ResponseEntity<>(sequenzData, HttpStatus.OK);
    }

 /*   @GetMapping(path = "/distinct/")
    public ResponseEntity<List<sequenzLabelData>> getDistinctLabels() {
        List<sequenzLabelData> labels = sequenzDataService.getDistinctLabels();
        return new ResponseEntity<>(labels, HttpStatus.OK);
    }
*/

    @GetMapping(path = "/distinct/{gid}")
    public ResponseEntity<List<String>> getDistinctLabelsById(@PathVariable int gid) {
        List<String> labels = sequenzDataService.getDistinctLabelsById(gid);
        return new ResponseEntity<>(labels, HttpStatus.OK);
    }

    @PostMapping(path = "/add")
    public ResponseEntity<Map<String, String>> addSequenzData(SequenzData sequenzData) {
        sequenzData.setManual(true);
        sequenzDataService.addSequenzData(sequenzData);
        Map<String, String> response = new HashMap<>();
        response.put("added", "true");
        response.put("message", "SequenzData added successfully");
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PostMapping(path ="/add/betweentime")
    public ResponseEntity<Map<String, String>> addBetweenTime(@RequestBody Map<String, String> body) {
        int gid = Integer.parseInt(body.get("gid"));
        double starttime = Double.parseDouble(body.get("starttime"));
        double endtime = Double.parseDouble(body.get("endtime"));
        String slabel = body.get("slabel");

        Map<String, String> response = new HashMap<>();
        if(sequenzDataService.addSequenzDataBetweenTime(gid, starttime, endtime, true, slabel)) {
            response.put("added", "true");
            response.put("message", "Sequenz wurde hinzugefügt");
        }else {
            response.put("added", "false");
            response.put("message", "Sequenz konnte nicht hinzugefügt werden.");
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(path = "/initial")
    public ResponseEntity<Map<String, String>> initialSequenzData() {
        Map<String, String> response = new HashMap<>();
        if(sequenzDataService.existByManual(false)) {
            response.put("initial", "false");
            response.put("message", "Es exestieren bereits Automatisch erstellte Sequenzdaten" +
                    " in der Datenbank. Bitte löschen Sie diese, bevor Sie die Initialisierung durchführen.");
        }else {
            sequenzIdentification.start(0, System.currentTimeMillis()/1000);
            response.put("initial", "true");
            response.put("message", "Es exestieren noch keine Automatisch erstellte Sequenzdaten. " +
                    "Initialisierung wird gestartet");
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

}
