package swtp.backend.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import swtp.backend.db.log.hourlyData.HourlyData;
import swtp.backend.db.log.hourlyData.HourlyDataService;

import java.util.List;

/**
 * RestController (api) für die Daten der letzten Stunde (HourlyData)
 */



@RestController
@RequestMapping(path ="/api/v1/hourly")
public class HourlyDataController {

    private final HourlyDataService hourlyDataService;

    @Autowired
    public HourlyDataController(HourlyDataService hourlyDataService){
        this.hourlyDataService = hourlyDataService;
    }

    /**
     * Gibt die stündlichen Statistischen Daten zurück
     *
     * @return Liste mit den Daten der einzelnen Stunden
     */
    @GetMapping
    public List<HourlyData> getHourlyData(){
        return hourlyDataService.getHourlyData();
    }

    @GetMapping(path = "/{gid}")
    public List<HourlyData> getHourlyDataById(@PathVariable("gid") int gid){
        return hourlyDataService.getHourlyDataById(gid);
    }

    @GetMapping(path = "/time")
    public List<HourlyData> getHourlyDataWithTime(@RequestParam double startTime, @RequestParam double endTime){
        return hourlyDataService.getHourlyDataWithTime(startTime,endTime);
    }

    @GetMapping(path = "/{gid}/time")
    public List<HourlyData> getHourlyDataByIdAndTime(@PathVariable("gid") int gid, @RequestParam double startTime, @RequestParam double endTime){
        return hourlyDataService.getHourlyDataWithIdAndTime(gid, startTime, endTime);
    }

}
