package swtp.backend.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import swtp.backend.db.log.weeklyData.WeeklyData;
import swtp.backend.db.log.weeklyData.WeeklyDataService;

import java.util.List;

/**
 * RestController (api) für die Daten der letzten 7 Tage (WeeklyData)
 */


@RestController
@RequestMapping(path ="/api/v1/weekly")
public class WeeklyDataController {

    private final WeeklyDataService weeklyDataService;

    @Autowired
    public WeeklyDataController(WeeklyDataService weeklyDataService){
        this.weeklyDataService = weeklyDataService;
    }

    /**
     * Gibt die Wöchentlichen Statistischen Daten zurück
     *
     * @return Liste von WeeklyData
     */
    @GetMapping
    public List<WeeklyData> getWeeklyData(){
        return weeklyDataService.getWeeklyData();
    }

    @GetMapping(path = "/{gid}")
    public List<WeeklyData> getWeeklyDataByGid(@PathVariable("gid") int gid){
        return weeklyDataService.getWeeklyDataByGid(gid);
    }

    @GetMapping(path = "/time")
    public List<WeeklyData> getWeeklyDataWithTime(@RequestParam double startTime, @RequestParam double endTime){
        return weeklyDataService.getWeeklyDataWithTime(startTime,endTime);
    }


    @GetMapping(path = "/{gid}/time")
    public List<WeeklyData> getWeeklyDataByGidAndTime(@PathVariable("gid") int gid, @RequestParam double startTime, @RequestParam double endTime){
        return weeklyDataService.getWeeklyDataWithGidAndTime(gid, startTime, endTime);
    }
}
