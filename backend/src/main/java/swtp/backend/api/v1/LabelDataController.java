package swtp.backend.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import swtp.backend.db.devicesData.DevicesDataService;
import swtp.backend.db.labelSystem.devicesLabelData.DevicesLabelData;
import swtp.backend.db.labelSystem.devicesLabelData.DevicesLabelDataService;
import swtp.backend.db.labelSystem.labelData.LabelData;
import swtp.backend.db.labelSystem.labelData.LabelDataService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * RestController (api) für die label Daten.
 */

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(path ="/api/v1/label")
public class LabelDataController {
    private final LabelDataService labelDataService;
    private final DevicesLabelDataService devicesLabelDataService;
    private final DevicesDataService devicesDataService;

    @Autowired
    public LabelDataController(LabelDataService labelDataService, DevicesLabelDataService devicesLabelDataService,  DevicesDataService devicesDataService) {
        this.labelDataService = labelDataService;
        this.devicesLabelDataService = devicesLabelDataService;
        this.devicesDataService = devicesDataService;
    }

    @GetMapping
    public ResponseEntity<List<LabelData>> getLabelData(){
        List<LabelData> lds = labelDataService.getLabelData();
        if(lds.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(lds, HttpStatus.OK);
    }

    @GetMapping(path = "/{lid}")
    public ResponseEntity<LabelData> getLabelDataById(@PathVariable int lid){
        LabelData ld = labelDataService.getLabelDataById(lid);
        if(ld == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(ld, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<LabelData> addLabelData(@RequestBody LabelData labelData){
        labelDataService.addLabelData(labelData);
        return new ResponseEntity<>(labelData, HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/{lid}")
    public Map<String, String> removeLabelData(@PathVariable int lid, @RequestParam(defaultValue = "false") boolean force){
        Map<String, String> response = new HashMap<>();

        LabelData ld = labelDataService.getLabelDataById(lid);
        if(ld == null){
            response.put("deleted", "false");
            response.put("message", "LabelData with lid " + lid + " not found");
        }else {
            if(force){
                labelDataService.removeLabelData(lid);
                response.put("deleted", "true");
                response.put("message", "LabelData with lid " + lid + " forced deleted");
            }else {
                if(devicesLabelDataService.existsByLid(lid)){
                    response.put("deleted", "false");
                    response.put("message", "Label is still assigned to a device");
                }else {
                    labelDataService.removeLabelData(lid);
                    response.put("deleted", "true");
                    response.put("message", "LabelData with lid " + lid + " deleted");
                }
            }
        }
        return response;
    }

    @PostMapping(path = "/addDevice")
    public ResponseEntity<DevicesLabelData> addDeviceToLabel(@RequestBody DevicesLabelData devicesLabelData){

        // Checke ob Device schon diesen Label zugeordnet ist
        if(devicesLabelDataService.existsByGidAndLid(devicesLabelData.getGid(), devicesLabelData.getLid())){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        // Checke ob Device mit gid existiert und Label mit lid existiert
        if(!devicesDataService.existsByGid(devicesLabelData.getGid())
                || !labelDataService.existsByLid(devicesLabelData.getLid())){
            devicesLabelData.setGid(-1);
            devicesLabelData.setLid(-1);
            return new ResponseEntity<>(devicesLabelData, HttpStatus.NOT_FOUND);
        }

        //Checke ob Device mit gid existiert
        if(!devicesDataService.existsByGid(devicesLabelData.getGid())){
            devicesLabelData.setGid(-1);
            return new ResponseEntity<>(devicesLabelData, HttpStatus.NOT_FOUND);
        }

        //Checke ob Label mit lid existiert
        if(!labelDataService.existsByLid(devicesLabelData.getLid())){
            devicesLabelData.setLid(-1);
            return new ResponseEntity<>(devicesLabelData, HttpStatus.NOT_FOUND);
        }

        this.devicesLabelDataService.addDeviceToLabel(devicesLabelData);
        return new ResponseEntity<>(devicesLabelData, HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/{lid}/remove/{gid}")
    public Map<String, Boolean> removeDeviceFromLabel(@PathVariable int gid, @PathVariable int lid){
        Map<String, Boolean> map = new HashMap<>();

        // Checke ob Device schon diesen Label zugeordnet ist
        if(devicesLabelDataService.existsByGidAndLid(gid, lid)){
            devicesLabelDataService.removeLabelFromDevice(gid, lid);
            map.put("removed", Boolean.TRUE);
        }else {
            map.put("removed", Boolean.FALSE);
        }
        return map;
    }

    @GetMapping(path = "/{lid}/devices")
    public ResponseEntity<List<DevicesLabelData>> getDevicesByLabel(@PathVariable int lid){
        List<DevicesLabelData> devicesLabelData = devicesLabelDataService.getDevicesByLabel(lid);
        if(devicesLabelData.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(devicesLabelData, HttpStatus.OK);
    }
}
