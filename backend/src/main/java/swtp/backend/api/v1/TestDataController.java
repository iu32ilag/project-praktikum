package swtp.backend.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import swtp.backend.db.log.stagingData.StagingData;
import swtp.backend.db.log.stagingData.StagingDataRep;
import swtp.backend.utils.ScheduleJobs;
import swtp.backend.utils.helper.SequenzIdentification;



@CrossOrigin(origins = "*")
@RestController
@RequestMapping(path ="/api/v1/test")
public class TestDataController {

    @Autowired
    private SequenzIdentification sq;
    @Autowired
    private StagingDataRep sd;
    @Autowired
    private ScheduleJobs sj;

    @GetMapping
    public String getTestData(){
        sq.start((System.currentTimeMillis() / 1000) - 10800 , System.currentTimeMillis() / 1000);
        return "Test";
    }

    @GetMapping(path = "/start")
    public String start(){
        sj.hourlyJob();
        return "Start";
    }

    @GetMapping(path = "/testData")
    private void createTestData() {
        int gid = 3;
        int wert = 80;
        int Eintraege = 31;
        for(int i = 0; i < Eintraege; i++) {
            sd.save(new StagingData(gid, i, wert));
        }

    }
}
