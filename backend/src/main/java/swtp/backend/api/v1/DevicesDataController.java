package swtp.backend.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import swtp.backend.db.devicesData.DevicesData;
import swtp.backend.db.devicesData.DevicesDataService;
import swtp.backend.db.labelSystem.devicesLabelData.DevicesLabelDataService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * RestController (api) für die Devices Daten (DailyData)
 */

@RestController
@RequestMapping(path ="/api/v1/devices")
public class DevicesDataController {

    private final DevicesDataService devicesDataService;
    private final DevicesLabelDataService devicesLabelDataService;

    @Autowired
    public DevicesDataController(DevicesDataService devicesDataService, DevicesLabelDataService devicesLabelDataService) {
        this.devicesDataService = devicesDataService;
        this.devicesLabelDataService = devicesLabelDataService;
    }

    /**
     * Gibt alle Geratedaten zurueck
     * @return Liste aller Geratedaten
     */
    @GetMapping
    public ResponseEntity<List<DevicesData>> getDevices(){
        List<DevicesData> dds = devicesDataService.getDevices();
        return new ResponseEntity<>(dds, HttpStatus.OK);
    }

    /**
     * Gibt die Geratedaten eines Gerates zurueck
     * @param id ID des Gerates
     * @return Geratedaten des Gerates
     */
    @GetMapping(path = "/{id}/all")
    public DevicesData getDevicesData(@PathVariable int id){
        return devicesDataService.getDevicesDataByGid(id);
    }



    /**
     * Gibt alle Geräte die active oder inactive sind zurück
     *
     * @return Liste aller Geräte die active sind
     */
    @GetMapping(path = "/status/active/{status}")
    public List<DevicesData> getActiveDevices(@PathVariable String status){
        if(status.equals("true")) {
            return devicesDataService.getActiveDevices();
        } else if(status.equals("false")) {
            return devicesDataService.getInactiveDevices();
        }else {
            List<DevicesData> ldd = new ArrayList<>();
            DevicesData dd = new DevicesData();
            dd.setGid(-1);
            ldd.add(dd);
            return ldd;
        }
    }

    /**
     * Gibt alle Geräte die im Idle sind zurück
     *
     * @return Liste aller Geräte die im idle sind
     */
    @GetMapping(path = "/status/idle")
    public List<DevicesData> getIdleDevices() {
        return devicesDataService.getIdleDevices();
    }


    /** Gibt Geräte zurück die länger nicht gesehen wurden
     * @param lastSeen Zeitpunkt der letzten Sichtung
     * @return Liste mit Geräten die länger nicht gesehen wurden
     */
    @GetMapping(path = "/lastSeen/{lastSeen}")
    public List<DevicesData> getDevicesDataByLastSeen(@PathVariable double lastSeen){
        return devicesDataService.getDevicesDataByLastSeen(lastSeen);
    }

    /**
     * Gibt den Letzten Verbrauchswert eines Gerates zurueck
     * @param id ID des Gerates
     * @return Letzter Verbauchswert des Gerates
     */
    @GetMapping(path = "/{id}/last")
    public double getLastDevicesData(@PathVariable int id){
        return devicesDataService.getLastDevicesDataById(id);
    }


    @GetMapping(path = "/count/idle/{isIdle}")
    public int countDevicesDataByisIdle(@PathVariable boolean isIdle){
        return devicesDataService.countDevicesDataByisIdle(isIdle);
    }

    @DeleteMapping(path = "/{gid}/remove/{lid}")
    public Map<String, Boolean> removeDeviceFromLabel(@PathVariable int gid, @PathVariable int lid){
        Map<String, Boolean> map = new HashMap<>();

        // Checke ob Device schon diesen Label zugeordnet ist
        if(devicesLabelDataService.existsByGidAndLid(gid, lid)){
            devicesLabelDataService.removeLabelFromDevice(gid, lid);
            map.put("removed", Boolean.TRUE);
        }else {
            map.put("removed", Boolean.FALSE);
        }
        return map;
    }
}
