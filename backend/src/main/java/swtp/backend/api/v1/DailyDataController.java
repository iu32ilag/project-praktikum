package swtp.backend.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import swtp.backend.db.log.dailyData.DailyData;
import swtp.backend.db.log.dailyData.DailyDataService;

import java.util.List;

/**
 * RestController (api) für die Daten der letzten 24 Stunden (DailyData)
 */

@RestController
@RequestMapping(path ="/api/v1/daily")
public class DailyDataController {

    private final DailyDataService dailyDataService;

    @Autowired
    public DailyDataController(DailyDataService dailyDataService){
        this.dailyDataService = dailyDataService;
    }

    /**
     * Gibt die täglichen Statistischen Daten zurück
     *
     * @return Liste von DailyData
     */
    @GetMapping
    public List<DailyData> getDailyData(){
        return dailyDataService.getDailyData();
    }

    @GetMapping(path = "/{gid}")
    public List<DailyData> getDailyDataByGid(@PathVariable("gid") int gid){
        return dailyDataService.getDailyDataByGid(gid);
    }

    @GetMapping(path = "/time")
    public List<DailyData> getDailyDataWithTime(@RequestParam double startTime, @RequestParam double endTime){
        return dailyDataService.getDailyDataWithTime(startTime,endTime);
    }

    @GetMapping(path = "/{gid}/time")
    public List<DailyData> getDailyDataByGidAndTime(@PathVariable("gid") int gid, @RequestParam double startTime, @RequestParam double endTime){
        return dailyDataService.getDailyDataWithGidAndTime(gid, startTime, endTime);
    }
}
