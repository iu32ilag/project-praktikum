package swtp.backend.db.sequenz;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import swtp.backend.utils.sequenzLabelData;

import java.util.List;

@Repository
public interface SequenzDataRep extends JpaRepository<SequenzData, Long> {

    List<SequenzData> findByGid(int gid);

    List<SequenzData> findBySlabel(String slabel);

    List<SequenzData> findByGidAndSlabel(int gid, String slabel);

    @Query(value = "SELECT DISTINCT slabel AS slabel FROM SequenzData WHERE gid = ?1")
    List<String> findDistinctSlabelByGid(int gid);


    @Query("SELECT DISTINCT gid AS gid, slabel AS slabel FROM SequenzData ORDER ORDER BY gid")
    List<sequenzLabelData> findDistinctSlabel();

    boolean existsByManual(boolean manual);

}
