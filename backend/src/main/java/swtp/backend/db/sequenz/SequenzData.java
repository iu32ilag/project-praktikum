package swtp.backend.db.sequenz;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name= "sequenz_data",indexes = {@Index(name = "idx", columnList = "slabel,gid")})
@Getter
@Setter
@Builder
public class SequenzData implements Serializable {

    @Column(name = "sid")
    @Id
    @GeneratedValue
    private long sid;

    @Column(name = "slabel")
    private String slabel;

    @Column(name = "gid")
    private int gid;

    @Column(name = "value")
    private double value;

    @Column(name = "time")
    private double time;

    @Column(name = "manual")
    private boolean manual;

    public SequenzData() {

    }
    public SequenzData(long sid, String slabel, int gid, double value, double time, boolean manual) {
        this.sid = sid;
        this.slabel = slabel;
        this.gid = gid;
        this.value = value;
        this.time = time;
        this.manual = manual;
    }

    @Override
    public String toString() {
        return "SequenzData [sid=" + sid +
                ", slabel=" + slabel + ", " +
                "gid=" + gid + ", " +
                "value=" + value + ", " +
                "time=" + time + ", " +
                "manual=" + manual +
                "]";
    }
}
