package swtp.backend.db.sequenz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import swtp.backend.db.log.stagingData.StagingData;
import swtp.backend.db.log.stagingData.StagingDataService;
import swtp.backend.utils.sequenzLabelData;

import java.util.List;

@Service
public class SequenzDataService {
    @Autowired
    SequenzDataRep sequenzDataRep;
    @Autowired
    StagingDataService sds;

    public List<SequenzData> getSequenzData() {
        return sequenzDataRep.findAll();
    }

    public List<SequenzData> getSequenzDataById(int gid) {
        return sequenzDataRep.findByGid(gid);
    }

    public List<SequenzData> getSequenzDataByLabel(String slabel) {
        return sequenzDataRep.findBySlabel(slabel);
    }

    public List<SequenzData> getSequenzDataByIdAndLabel(int gid, String slabel) {
        return sequenzDataRep.findByGidAndSlabel(gid, slabel);
    }

    public List<sequenzLabelData> getDistinctLabels() {
        return sequenzDataRep.findDistinctSlabel();
    }

    public List<String> getDistinctLabelsById(int gid) {
        return sequenzDataRep.findDistinctSlabelByGid(gid);
    }

    public SequenzData addSequenzData(SequenzData sequenzData){
        return sequenzDataRep.save(sequenzData);
    }

    public boolean existByManual(boolean manual){
        return sequenzDataRep.existsByManual(manual);
    }

    public boolean addSequenzDataBetweenTime(int gid, double starttime, double endtime, boolean manual, String label) {
        //TODO: hinzufügen verschiedener Checks
        List<StagingData> rawList = sds.findStagingDataByTimeBetweenAndGid(starttime, endtime, gid);
        for(StagingData stagingData: rawList) {
            SequenzData sequenzData = new SequenzData();
            sequenzData.setManual(manual);
            sequenzData.setSlabel(label);
            sequenzData.setTime(starttime);
            sequenzData.setGid(gid);
            sequenzData.setValue(stagingData.getValue());
            addSequenzData(sequenzData);
        }
        return true;
    }
}
