package swtp.backend.db.labelSystem.labelData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LabelDataService {
    @Autowired
    LabelDataRep labelDataRep;

    public LabelData addLabelData(LabelData labelData){
        return labelDataRep.save(labelData);
    }

    public void removeLabelData(int lid){
        labelDataRep.deleteByLid(lid);
    }

    public List<LabelData> getLabelData(){
        return labelDataRep.findAll();
    }

    public LabelData getLabelDataById(int lid){
        return labelDataRep.findLabelDataBylid(lid);
    }

    public Boolean existsByLid(int lid){
        return labelDataRep.existsByLid(lid);
    }
}
