package swtp.backend.db.labelSystem.devicesLabelData;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@IdClass(DevicesLabelDataId.class)
@Table(name = "devices_label_data")
@Getter
@Setter
@Builder
public class DevicesLabelData implements Serializable {
    @Column(name = "gid", nullable = false)
    @Id
    private int gid;

    @Column(name = "lid", nullable = false)
    @Id
    private int lid;

    public DevicesLabelData() {
    }

    public DevicesLabelData(int gid, int lid) {
        this.gid = gid;
        this.lid = lid;
    }

    @Override
    public String toString() {
        return "LabelsCatData [gid=" + gid + ", lid=" + lid + "]";
    }
}
