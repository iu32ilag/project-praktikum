package swtp.backend.db.labelSystem.devicesLabelData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DevicesLabelDataService {
    @Autowired
    private DevicesLabelDataRep devicesLabelDataRep;

    public DevicesLabelData addDeviceToLabel(DevicesLabelData devicesLabelData){
        return devicesLabelDataRep.save(devicesLabelData);
    }

    public Boolean existsByGidAndLid(int gid, int lid){
        return devicesLabelDataRep.existsByGidAndLid(gid, lid);
    }

    public Boolean existsByLid(int lid){
        return devicesLabelDataRep.existsByLid(lid);
    }

    public void removeLabelFromDevice(int gid, int lid){
        devicesLabelDataRep.deleteByGidAndLid(gid, lid);
    }

    public List<DevicesLabelData> getDevicesByLabel(int lid){
        return devicesLabelDataRep.getDevicesByLid(lid);
    }

}
