package swtp.backend.db.labelSystem.labelData;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "label_data")
@Getter
@Setter
@Builder
public class LabelData implements Serializable {
    @Column(name = "lid")
    @Id
    @GeneratedValue
    private int lid;

    @Column(name = "Name", nullable = false, length = 50)
    private String name;

    @Column(name = "Description")
    private String description;

    @Column(name = "userCreated", nullable = false, columnDefinition = "boolean default false")
    private boolean userCreated;


    public LabelData() {
    }
    public LabelData(int lid, String name, String description, boolean userCreated) {
        this.lid = lid;
        this.name = name;
        this.description = description;
        this.userCreated = userCreated;
    }

    @Override
    public String toString() {
        return "LabelData [lid=" + lid +
                ", name=" + name + ", " +
                "description=" + description + ", " +
                "userCreated=" + userCreated +
                "]";
    }
}
