package swtp.backend.db.labelSystem.devicesLabelData;

import java.io.Serializable;
import java.util.Objects;

public class DevicesLabelDataId implements Serializable {
    private int gid;
    private int lid;

    public DevicesLabelDataId(){}

    public DevicesLabelDataId(int gid, int lid) {
        this.gid = gid;
        this.lid = lid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DevicesLabelDataId that)) return false;
        return gid == that.gid && Double.compare(that.lid, lid) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(gid, lid);
    }

}
