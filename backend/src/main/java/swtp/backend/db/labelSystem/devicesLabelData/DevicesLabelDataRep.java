package swtp.backend.db.labelSystem.devicesLabelData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface DevicesLabelDataRep extends JpaRepository<DevicesLabelData, DevicesLabelDataId> {
    Boolean existsByGidAndLid(int gid, int lid);
    Boolean existsByLid(int lid);

    @Transactional
    void deleteByGidAndLid(int gid, int lid);

    List<DevicesLabelData> getDevicesByLid(int lid);
}
