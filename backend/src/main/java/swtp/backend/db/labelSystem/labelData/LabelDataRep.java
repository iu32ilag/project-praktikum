package swtp.backend.db.labelSystem.labelData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface LabelDataRep extends JpaRepository<LabelData, LabelDataId> {

    LabelData findLabelDataBylid(int lid);

    @Transactional
    void deleteByLid(int lid);

    Boolean existsByLid(int lid);
}
