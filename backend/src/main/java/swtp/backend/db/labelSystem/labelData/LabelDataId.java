package swtp.backend.db.labelSystem.labelData;


import java.util.Objects;

public class LabelDataId {
    private int lid;
    private double time;
    public LabelDataId(){}

    public LabelDataId(int lid, double time) {
        this.lid = lid;
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LabelDataId that)) return false;
        return lid == that.lid && Double.compare(that.time, time) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(lid, time);
    }
}
