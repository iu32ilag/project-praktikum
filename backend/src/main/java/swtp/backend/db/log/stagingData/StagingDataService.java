package swtp.backend.db.log.stagingData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import swtp.backend.utils.DataWithValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class StagingDataService {
    @Autowired
    StagingDataRep stagingDataRep;

    /**
     * Gibt Liste alle StagingData zurück
     *
     * @return Liste aller StagingData
     */
    public List<StagingData> getStagingData(){
        return stagingDataRep.findAll();
    }

    public List<StagingData> getStagingDataByGid(int gid){
        return stagingDataRep.findByGid(gid);
    }

    public List<StagingData> findStagingDataByTimeBetweenAndGid(double startTime, double endTime, int gid){
        return stagingDataRep.findStagingDataByTimeBetweenAndGid(startTime,endTime, gid);
    }

    /**
     * Gibt Liste alle StagingData zurück, die zwischen den beiden Zeitpunkten liegen
     *
     * @param startTime Startzeitpunkt der StagingData
     * @param endTime   Endzeitpunkt der StagingData
     * @return Liste aller StagingData zwischen den beiden Zeitpunkten
     */
    public List<StagingData> getStagingDataWithTime(double startTime, double endTime){
        return stagingDataRep.findStagingDataByTimeBetween(startTime, endTime);
    }

    public void deleteStagingDataByGidAndTimeBefore(int gid, double time){
        this.stagingDataRep.deleteAllByGidAndTimeBefore(gid, time);
    }

    public void deleteStagingDataBefore(double time){
        this.stagingDataRep.deleteAllByTimeBefore(time);
    }


    /**
     * Gruppiert die Daten nach Geräten
     *
     * @return Map mit der ID des Gerätes als Key und einer Liste der Staging Daten als Value
     */
    @Deprecated
    public Map<Integer, List<DataWithValue>> groupStagingDataByDevice(List<StagingData> data){

        Map<Integer, List<DataWithValue>> map = new java.util.HashMap<>();

        for(StagingData d : data){
            if(map.containsKey(d.getGid())) {
                List<DataWithValue> list = map.get(d.getGid());
                list.add(d);
                map.put(d.getGid(), list);
            }else {
                List<DataWithValue> list = new ArrayList<>();
                list.add(d);
                map.put(d.getGid(), list);
            }
        }
        return map;
    }
}
