package swtp.backend.db.log.dailyData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import swtp.backend.utils.DataWithValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class DailyDataService {
    @Autowired
    private DailyDataRep dailyDataRep;
    /**
     * Gibt die kompletten Daten der Tabelle dailyData zurück
     *
     * @return Liste mit allen Daten der Tabelle dailyData
     */
    public List<DailyData> getDailyData(){
        return dailyDataRep.findAll();
    }

    public List<DailyData> getDailyDataByGid(int gid){
        return dailyDataRep.findDailyDataByGid(gid);
    }

    public List<DailyData> getDailyDataWithTime(double startTime, double endTime){
        return dailyDataRep.findDailyDataByTimeBetween(startTime, endTime);
    }

    public List<DailyData> getDailyDataWithGidAndTime(int gid, double startTime, double endTime){
        return dailyDataRep.findDailyDataByTimeBetweenAndGid(startTime, endTime, gid);
    }

    public Map<Integer, List<DataWithValue>> groupDailyDataByDevice(List<DailyData> data){

        Map<Integer, List<DataWithValue>> map = new java.util.HashMap<>();

        for(DailyData d : data){
            if(map.containsKey(d.getGid())) {
                List<DataWithValue> list = map.get(d.getGid());
                list.add(d);
                map.put(d.getGid(), list);
            }else {
                List<DataWithValue> list = new ArrayList<>();
                list.add(d);
                map.put(d.getGid(), list);
            }
        }
        return map;
    }
}
