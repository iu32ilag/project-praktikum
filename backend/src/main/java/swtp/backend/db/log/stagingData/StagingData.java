package swtp.backend.db.log.stagingData;

import lombok.Getter;
import lombok.Setter;
import swtp.backend.utils.DataWithValue;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@IdClass(StagingDataId.class)
@Table(name = "staging_data")
@Getter
@Setter
public class StagingData extends DataWithValue implements Serializable{

    @Column(name = "gid")
    @Id
    private int gid;

    @Column(name = "time")
    @Id
    private double time;

    @Column(name = "value")
    private double value;

    @Transient // idle Time soll nicht in DB gespeichert werden und auch nicht geändert werden
    private double idleTime = 0;
    public StagingData() {
    }

    public StagingData(int gid, double time, double value) {
        this.gid = gid;
        this.time = time;
        this.value = value;
    }

    @Override
    public double getValue(){
        return this.value;
    }

    public double getIdleTime() {
        return idleTime;
    }

    @Override
    public String toString() {
        return "StagingData [gid=" + gid + ", time=" + time + ", value=" + value + "]";
    }
}
