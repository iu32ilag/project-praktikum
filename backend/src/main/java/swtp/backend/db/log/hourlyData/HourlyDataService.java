package swtp.backend.db.log.hourlyData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import swtp.backend.utils.DataWithValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class HourlyDataService {
    @Autowired
    private HourlyDataRep hourlyDataRep;

    /**
     * Gibt alle Daten aus der Datenbank hourlyData zurück
     *
     * @return Liste mit allen Hourly Daten
     */
    public List<HourlyData> getHourlyData(){
        return hourlyDataRep.findAll();
    }

    public List<HourlyData> getHourlyDataById(int gid){
        return hourlyDataRep.findHourlyDataByGid(gid);
    }

    public List<HourlyData> getHourlyDataWithTime(double startTime, double endTime){
        return hourlyDataRep.findHourlyDataByTimeBetween(startTime, endTime);
    }

    public List<HourlyData> getHourlyDataWithIdAndTime(int gid, double startTime, double endTime){
        return hourlyDataRep.findHourlyDataByTimeBetweenAndGid(startTime, endTime, gid);
    }

    public Map<Integer, List<DataWithValue>> groupHourlyDataByDevice(List<HourlyData> data){

        Map<Integer, List<DataWithValue>> map = new java.util.HashMap<>();

        for(HourlyData d : data){
            if(map.containsKey(d.getGid())) {
                List<DataWithValue> list = map.get(d.getGid());
                list.add(d);
                map.put(d.getGid(), list);
            }else {
                List<DataWithValue> list = new ArrayList<>();
                list.add(d);
                map.put(d.getGid(), list);
            }
        }
        return map;
    }
}
