package swtp.backend.db.log.hourlyData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HourlyDataRep extends JpaRepository<HourlyData, HourlyDataId>{
    List<HourlyData> findHourlyDataByGid(int id);

    List<HourlyData> findHourlyDataByTimeBetween(double startTime, double endTime);

    List<HourlyData> findHourlyDataByTimeBetweenAndGid(double startTime, double endTime, int gid);

}
