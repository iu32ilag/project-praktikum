package swtp.backend.db.log.dailyData;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import swtp.backend.utils.TimeData;

import javax.persistence.*;
import java.io.Serializable;

@Entity

/*für den Composite Key bestehend aus gid und Zeitstempel*/
@IdClass(DailyDataId.class)
@Table(name = "daily_data")
@Getter
@Setter
@Builder
public class DailyData extends TimeData implements Serializable {
    @Column(name = "gid")
    @Id
    private int gid;
    @Column(name = "time") // Zeitstempel zur vollen Stunde
    @Id
    private double time;

    @Column(name = "averageValue")
    private double value; // average value

    // sollten auch solche Werte gespeichert werden?
    @Column(name = "total")
    private double total;
    @Column(name = "peak")
    private double peak;
    @Column(name = "lowest")
    private double lowest;
    @Column(name = "idleTime")
    private double idleTime;

    public DailyData(){
    }

    public DailyData(int gid, double time, double value, double total, double peak, double lowest, double idleTime) {
        this.gid = gid;
        this.time = time;
        this.value = value;
        this.total = total;
        this.peak = peak;
        this.lowest = lowest;
        this.idleTime = idleTime;
    }

    @Override
    public String toString() {
        return "DailyData{" +
                "gid=" + gid +
                ", time=" + time +
                ", value=" + value +
                ", total=" + total +
                ", peak=" + peak +
                ", lowest=" + lowest +
                ", idleTime=" + idleTime +
                '}';
    }
}
