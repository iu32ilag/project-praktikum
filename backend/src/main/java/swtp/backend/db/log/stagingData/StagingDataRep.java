package swtp.backend.db.log.stagingData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface StagingDataRep extends JpaRepository<StagingData, StagingDataId> {

    List<StagingData> findByGid(int gid);

    List<StagingData> findStagingDataByTimeBetweenAndGid(double startTime, double endTime, int gid);

    /**
     * Gibt Liste alle StagingData zurück, die zwischen den beiden Zeitpunkten liegen
     *
     * @param startTime Startzeitpunkt der StagingData
     * @param endTime Endzeitpunkt der StagingData
     * @return Liste aller StagingData zwischen den beiden Zeitpunkten
     */
    //@Query("SELECT s FROM StagingData s WHERE s.time > ?1 and s.time < ?2") macht dasselbe wie findStagingDataByTimeBetween
    List<StagingData> findStagingDataByTimeBetween(double startTime, double endTime);

    /**
     * Löscht alle Einträge aus der Tabelle vor dem angegebenen Zeitpunkt
     *
     * @param time Unixtime Zeitpunkt bis zu dem die Einträge gelöscht werden sollen
     */
    @Transactional
    void deleteAllByTimeBefore(double time);

    @Transactional
    void deleteAllByGidAndTimeBefore(int gid, double time);
}
