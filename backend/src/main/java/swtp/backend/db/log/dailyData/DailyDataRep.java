package swtp.backend.db.log.dailyData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DailyDataRep extends JpaRepository<DailyData, DailyDataId>{
    List<DailyData> findDailyDataByGid(int gid);

    List<DailyData> findDailyDataByTimeBetween(double startTime, double endTime);

    List<DailyData> findDailyDataByTimeBetweenAndGid(double startTime, double endTime, int gid);

}
