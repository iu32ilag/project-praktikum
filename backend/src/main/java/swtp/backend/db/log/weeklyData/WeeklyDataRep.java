package swtp.backend.db.log.weeklyData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WeeklyDataRep extends JpaRepository<WeeklyData, WeeklyDataId>{
    List<WeeklyData> findWeeklyDataByGid(int id);

    List<WeeklyData> findWeeklyDataByTimeBetween(double startTime, double endTime);

    List<WeeklyData> findWeeklyDataByTimeBetweenAndGid(double startTime, double endTime, int id);
}
