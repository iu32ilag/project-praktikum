package swtp.backend.db.log.weeklyData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeeklyDataService {
    @Autowired
    private WeeklyDataRep weeklyDataRep;

    /**
     * Gibt Liste aller WeeklyData zurück
     *
     * @return Liste aller WeeklyData aus der Datenbank
     */
    public List<WeeklyData> getWeeklyData(){
        return weeklyDataRep.findAll();
    }

    public List<WeeklyData> getWeeklyDataByGid(int gid){
        return weeklyDataRep.findWeeklyDataByGid(gid);
    }

    public List<WeeklyData> getWeeklyDataWithTime(double startTime, double endTime){
        return weeklyDataRep.findWeeklyDataByTimeBetween(startTime, endTime);
    }

    public List<WeeklyData> getWeeklyDataWithGidAndTime(int gid, double startTime, double endTime){
        return weeklyDataRep.findWeeklyDataByTimeBetweenAndGid(startTime, endTime, gid);
    }
}
