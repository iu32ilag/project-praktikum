package swtp.backend.db.devicesData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DevicesDataService {
    @Autowired
    private DevicesDataRep devicesDataRep;

    public List<DevicesData> getDevices() {
        return devicesDataRep.findAll();
    }

    /**
     * Gibt die DevicesData für ein bestimmtes gid zurück
     *
     * @param gid gid des Devices
     * @return DevicesData für die gid
     */
    public DevicesData getDevicesDataByGid(int gid){
        return devicesDataRep.findDevicesDataBygid(gid);
    }

    /**
     * Gibt alle DevicesData zurück welche länger nicht gesehen wurden
     *
     * @param lastSeen Zeitpunkt der letzten Sichtung
     * @return Liste mit DevicesData welche länger nicht gesehen wurden
     */
    public List<DevicesData> getDevicesDataByLastSeen(double lastSeen){
        return devicesDataRep.findDevicesDataByLastSeenBefore(lastSeen);
    }

    /**
     * Gibt alle Devices zurück, die aktiv markiert sind
     *
     * @return Liste aller aktiven Devices
     */
    public List<DevicesData> getInactiveDevices(){
        return devicesDataRep.findDevicesDataByActive(false);
    }

    /**
     * Gibt alle Devices zurück, die inaktiv markiert sind
     *
     * @return Liste aller inaktiven Devices
     */
    public List<DevicesData> getActiveDevices(){
        return devicesDataRep.findDevicesDataByActive(true);
    }

    public List<DevicesData> getIdleDevices() {
        return devicesDataRep.findDevicesDataByisIdle(true);
    }

    /**
     *  Gibt den letzten Wert eines Devices mit bestimmter gid zurück
     *
     * @param gid gid des Devices
     * @return letzter Wert des Devices
     */
    public double getLastDevicesDataById(int gid){
        DevicesData dd = devicesDataRep.findDevicesDataBygid(gid);
        if(dd == null){
            return -1;
        }
        return devicesDataRep.findDevicesDataBygid(gid).getLastValue();
    }

    public boolean existsByGid(int gid){
        return devicesDataRep.existsByGid(gid);
    }

    public int countDevicesDataByisIdle(Boolean isIdle){
        return devicesDataRep.countDevicesDataByisIdle(isIdle);
    }

    public void addDevicesData(DevicesData devicesData){
        devicesDataRep.save(devicesData);
    }
    public void addDevicesData(int gid, String customID, boolean active, double firstSeen, double lastSeen, double idleTime, double peak, double average, double lowest, double allTime, double lastValue){
        devicesDataRep.save(new DevicesData(gid, customID, active, firstSeen, lastSeen, idleTime, peak, average, lowest, allTime, lastValue, false));
    }
    public void updateDevicesData(DevicesData devicesData) {
        devicesDataRep.save(devicesData);
    }
    public void updateDevicesData(int gid, String customID, boolean active, double lastSeen, double idleTime, double peak, double average, double lowest, double allTime, double lastValue, boolean isIdle){
        DevicesData dd = devicesDataRep.findDevicesDataBygid(gid);
        dd.setCustomID(customID);
        dd.setActive(active);
        dd.setLastSeen(lastSeen);
        dd.setIdleTime(idleTime);
        dd.setPeak(peak);
        dd.setAverage(average);
        dd.setLowest(lowest);
        dd.setAllTime(allTime);
        dd.setLastValue(lastValue);
        dd.setIdle(isIdle);
        devicesDataRep.save(dd);
    }
    public List<DevicesData> findDevicesDataByLastSeenAfter(double lastSeen) {
        return devicesDataRep.findDevicesDataByLastSeenAfter(lastSeen);
    }
}
