package swtp.backend.db.devicesData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface DevicesDataRep extends JpaRepository<DevicesData, DevicesDataId>{

    /**
     *  Gibt DevocesData zurück mit bestimmter gid
     *
     * @param gid gid des DevicesData
     * @return DevicesData mit gid
     */
    DevicesData findDevicesDataBygid(int gid);

    /**
     * Gibt alle DevicesData zurück welche länger nicht gesehen wurden
     *
     * @param lastSeen Zeitpunkt der letzten Sichtung
     * @return Liste mit DevicesData welche länger nicht gesehen wurden
     */
    List<DevicesData> findDevicesDataByLastSeenBefore(double lastSeen);

    /**
     * Gibt alle DevicesData zurück welche active oder inactive sind
     *
     * @param active true für active, false für inactive
     * @return Liste mit DevicesData welche active oder inactive sind
     */
    List<DevicesData> findDevicesDataByActive(boolean active);

    List<DevicesData> findDevicesDataByisIdle(boolean is_Idle);

    int countDevicesDataByisIdle(boolean is_Idle);

    Boolean existsByGid(int gid);

    List<DevicesData> findDevicesDataByLastSeenAfter(double lastSeen);
}
