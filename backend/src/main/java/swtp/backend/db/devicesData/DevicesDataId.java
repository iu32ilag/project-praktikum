package swtp.backend.db.devicesData;

import java.io.Serializable;
import java.util.Objects;

public class DevicesDataId implements Serializable {
    private int gid;
    private double time;
    public DevicesDataId(){}

    public DevicesDataId(int gid, double time) {
        this.gid = gid;
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DevicesDataId that)) return false;
        return gid == that.gid && Double.compare(that.time, time) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(gid, time);
    }
}
