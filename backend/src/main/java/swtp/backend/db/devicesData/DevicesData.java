package swtp.backend.db.devicesData;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "devices_data")
@Getter
@Setter
@Builder

public class DevicesData implements Serializable {
    @Column(name = "gid")
    @Id
    private int gid;

    @Column(name = "customID", length = 50)
    private String customID;

    @Column(name = "active", nullable = false, columnDefinition = "boolean default false")
    private boolean active;

    @Column(name = "firstSeen", nullable = false)
    private double firstSeen;

    @Column(name = "lastSeen", nullable = false)
    private double lastSeen;

    @Column(name = "idleTime", nullable = false, columnDefinition = "decimal(32,15) default 0")
    private double idleTime;

    @Column(name = "peak", nullable = false, columnDefinition = "decimal(32,7) default 0")
    private double peak;

    @Column(name = "average", nullable = false, columnDefinition = "decimal(32,7) default 0")
    private double average;

    @Column(name = "lowest", nullable = false, columnDefinition = "decimal(32,7) default 1000")
    private double lowest;

    @Column(name = "allTime", nullable = false, columnDefinition = "decimal(32,7) default 0")
    private double allTime;

    @Column(name = "lastValue", nullable = false, columnDefinition = "decimal(32,7) default 0")
    private double lastValue;

    @Column(name = "isIdle", nullable = false, columnDefinition = "boolean default false")
    private boolean isIdle;

    public DevicesData() {

    }
    public DevicesData(int gid, String customID, boolean active, double firstSeen, double lastSeen, double idleTime, double peak, double average, double lowest, double allTime, double lastValue, boolean isIdle) {
        this.gid = gid;
        this.customID = customID;
        this.active = active;
        this.firstSeen = firstSeen;
        this.lastSeen = lastSeen;
        this.idleTime = idleTime;
        this.peak = peak;
        this.average = average;
        this.lowest = lowest;
        this.allTime = allTime;
        this.lastValue = lastValue;
        this.isIdle = isIdle;
    }


    @Override
    public String toString() {
        return "DailyData{" +
                "gid=" + gid +
                ", customID=" + customID +
                ", active=" + active +
                ", firstSeen=" + firstSeen +
                ", lastSeen=" + lastSeen +
                ", idleTime=" + idleTime +
                ", peak=" + peak +
                ", average=" + average +
                ", lowest=" + lowest +
                ", allTime=" + allTime +
                ", lastValue=" + lastValue +
                ", isIdle=" + isIdle +
                '}';
    }
}
