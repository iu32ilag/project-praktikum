package swtp.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hauptklasse der Backend-Applikation
 */
@SpringBootApplication
public class BackendApplication {

    /**
     * Main-Methode
     *
     * @param args Argumente
     */
	public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);  // Startet die Spring-Application
     }


}