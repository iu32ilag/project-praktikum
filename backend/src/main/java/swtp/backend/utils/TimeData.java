package swtp.backend.utils;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public abstract class TimeData extends DataWithValue {

    private int gid;
    private double time;
    private double value; // average value
    private double total;
    private double peak;
    private double lowest;
    private double idleTime;
}
