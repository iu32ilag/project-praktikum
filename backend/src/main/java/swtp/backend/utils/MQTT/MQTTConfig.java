package swtp.backend.utils.MQTT;

import lombok.Getter;

public class MQTTConfig {
    @Getter private static final String SERVER = "pcai042.informatik.uni-leipzig.de";// Server-Adresse
    @Getter private static final int PORT = 1883;                                    // Port des MQTT-Servers
    @Getter private static final String USER = "swtp22";                             // MQTT-User
    @Getter private static final String PWD = "0jUkzJgxqrse5q";                      // Passwort für den MQTT-Server
    @Getter private static final String TOPIC = "edison/+/active_power_calculated";  // + ist Wildcard element, d.h. es wird jedes Topic was diesen aufbau hat abonniert
}
