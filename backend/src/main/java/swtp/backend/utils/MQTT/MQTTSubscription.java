package swtp.backend.utils.MQTT;

import org.eclipse.paho.client.mqttv3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import swtp.backend.utils.helper.RawDataService;

import javax.annotation.PostConstruct;
import java.util.logging.Logger;

/**
 * Klasse zum Subscriben auf Topics
 */
@Component
public class MQTTSubscription {
    Logger log = Logger.getLogger("MQTTConnection");
    private final MQTTConfig mqttConfig = new MQTTConfig();

    @Autowired
    private  RawDataService rawDataService;

    @Autowired
    private MQTTConnection connection;


    /**
     * Subscribt auf ein Topic
     *
     */
    @PostConstruct
    public void subscribe(){
        connection.getClient().setCallback(new MqttCallback() {
            public void connectionLost(Throwable cause) {
                log.warning("Verbindung zum MQTT-Server verloren. Versuche neu zu verbinden...");

                connection.connect();
            }

            // hier kommt hin, was passieren soll, wenn eine Nachricht empfangen wird
            public void messageArrived(String topic,
                                       MqttMessage message)
                    throws Exception {

                topic = topic.replaceAll("\\D+",""); // entfernt alles außer die Zahlen aus dem topic um an die GeräteID zu kommen
                // (Achtung 1 wird als 01 geschrieben)
                //System.out.println("topicID: " + topic + " " +message.toString());

                rawDataService.transmitRawData(Integer.parseInt(topic), message.toString()); // gibt die GeräteID und die empfangene Nachricht an die RawDataService weiter
            }

            public void deliveryComplete(IMqttDeliveryToken token) {
            }
        });
        subscribeToTopic(MQTTConfig.getTOPIC());
    }

    private void subscribeToTopic(String topic){
            try {
                connection.getClient().subscribe(topic);
            } catch (MqttException e) {
                log.warning("MQTT-Client konnte nicht verbunden werden");
            } finally {
                log.info("Subscribed to Topic: " + topic);
            }
    }
}
