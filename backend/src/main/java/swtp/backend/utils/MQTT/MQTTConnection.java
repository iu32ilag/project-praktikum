package swtp.backend.utils.MQTT;

import lombok.Getter;
import org.eclipse.paho.client.mqttv3.* ;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Klasse zur Verbindung des MQTT-Servers
 */
@Configuration
public class MQTTConnection {
    Logger log = Logger.getLogger("MQTTConnection");

    /**
     * Client für Verbindung zum MQTT-Server
     */
    @Getter private IMqttClient client;

    /**
     * Konstruktor für MQTTConnection
     *
     */
    public MQTTConnection() {
        String publisherId = UUID.randomUUID().toString();      // Generiert eine zufällige ID
        try {
            client = new MqttClient("tcp://" + MQTTConfig.getSERVER() + ":" + MQTTConfig.getPORT(), publisherId); // Erstellt einen MQTT-Client, noch nicht verbunden
        }catch (MqttException e) {
            e.printStackTrace();
        }
    }

    /**
     * Verbindet den MQTT-Client mit dem MQTT-Server
     *
     */
    @PostConstruct
    public void connect() {
        MqttConnectOptions options = new MqttConnectOptions();  // Erstellt eine neue Verbindungsoption
        options.setUserName(MQTTConfig.getUSER());                              // Setzt den Benutzernamen
        options.setPassword(MQTTConfig.getPWD().toCharArray());                 // Setzt das Passwort
        options.setAutomaticReconnect(true);                    // Setzt die automatische Wiederverbindung
        options.setCleanSession(true);                          // Setzt die Sitzung auf clean
        options.setConnectionTimeout(2);                       // Setzt die Verbindungstimeout auf 10 Sekunden

            try {
                client.connect(options);                            // Verbindet den MQTT-Client mit dem MQTT-Server
            } catch (MqttException e) {
                log.warning("MQTT-Client konnte nicht verbunden werden");
            }

        if(client.isConnected()) {
            log.info("Connected to MQTT-Server");
        }
    }
}