package swtp.backend.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import swtp.backend.db.devicesData.DevicesData;
import swtp.backend.db.devicesData.DevicesDataService;
import swtp.backend.db.log.dailyData.DailyData;
import swtp.backend.db.log.dailyData.DailyDataRep;
import swtp.backend.db.log.dailyData.DailyDataService;
import swtp.backend.db.log.hourlyData.HourlyData;
import swtp.backend.db.log.hourlyData.HourlyDataRep;
import swtp.backend.db.log.hourlyData.HourlyDataService;
import swtp.backend.db.log.stagingData.StagingData;
import swtp.backend.db.log.stagingData.StagingDataService;
import swtp.backend.db.log.weeklyData.WeeklyData;
import swtp.backend.db.log.weeklyData.WeeklyDataRep;
import swtp.backend.utils.helper.SequenzIdentification;
import swtp.backend.utils.statistics.StatisticService;
import swtp.backend.utils.statistics.StatisticsTimeDataService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Klasse zum automatischen Ausfuehren von Aufgaben
 */
@EnableScheduling
@Configuration
public class    ScheduleJobs {
    private final DevicesDataService devicesDataService;
    private final HourlyDataService hds;
    private final StatisticService statisticService;
    private final StagingDataService sds;
    private final DailyDataService dds;
    private final HourlyDataRep hdr;
    private final DailyDataRep ddr;
    private final WeeklyDataRep wdr;
    private final StatisticsTimeDataService statisticsTimeDataService;
    private final SequenzIdentification sequenzIdentification;
    private Map<Integer, List<SequenzValue>> oldSQMap;

    Logger log = Logger.getLogger("ScheduleJobs");

    /**
     * Konstruktor
     * @param sds StagingDataService
     * @param statisticService StatisticService
     * @param sequenzIdentification
     */
    @Autowired
    public ScheduleJobs(DevicesDataService devicesDataService, StagingDataService sds, HourlyDataService hds, DailyDataService dds, StatisticService statisticService, StatisticsTimeDataService statisticsTimeDataService, HourlyDataRep hdr, DailyDataRep ddr, WeeklyDataRep wdr, SequenzIdentification sequenzIdentification) {
        this.devicesDataService = devicesDataService;
        this.sds = sds;
        this.hds = hds;
        this.dds = dds;
        this.hdr = hdr;
        this.ddr = ddr;
        this.wdr = wdr;
        this.statisticService = statisticService;
        this.statisticsTimeDataService = statisticsTimeDataService;
        this.sequenzIdentification = sequenzIdentification;
    }

    /**
     * Fuehrt die Berechnung der Stundenwerte aus
     */
    @Scheduled(cron = "10 0 * * * *")
    public void hourlyJob(){
        log.info("Hourly Job started");

        long startTime = System.currentTimeMillis() / 1000  - 3600 - 10; // 1 Stunde und 10 Sekunden zur Sicherhei
        long endTime = System.currentTimeMillis() / 1000 - 10; // aktuelle Zeit minus 10 Sekunden zur Sicherheit


        // Hole alle Geräte
        List<DevicesData> devicesData = getDevices(startTime);

        for(DevicesData dd: devicesData){
            List<DataWithValue> rawData = getRawDataFromDatabase(dd.getGid(), startTime, endTime);
            addDataToDatabase(dd.getGid(), rawData, endTime);
        }

        //List<StagingData> data = sds.getStagingDataWithTime( startTime, endTime); // Daten aus der Datenbank holen

        // Map<Integer, List<DataWithValue>> dataMap = sds.groupStagingDataByDevice(data); // Gruppiert die Daten nach Geräten

        //addDataToDatabase(dataMap, endTime); // Daten in die Datenbank schreiben

        // Daten aus der Datenbank löschen die älter als 3 Stunden sind
        long time = System.currentTimeMillis() / 1000 - (7 * 24 * 60 * 60); // 1 Woche in Sekunden

        // Hole alle Geräte
        List<DevicesData> devicesDataForDelete = getDevices(0);

        for (DevicesData dd: devicesDataForDelete){
            sds.deleteStagingDataByGidAndTimeBefore(dd.getGid(), time); // Daten aus der Datenbank loeschen
            log.info("sds.deleteStagingDataBefore ausgeführt für Gerät: " + dd.getGid());
        }

       // sds.deleteStagingDataBefore(time); // Daten aus der Datenbank loeschen
        // log.info("sds.deleteStagingDataBefore ausgeführt");

        log.info("Sequential Job started");
        sequenzIdentification.start(startTime, endTime);
        log.info("Sequential Job finished");
        log.info("Hourly Job finished");
    }

    /**
     * Fuehrt die Berechnung der Tageswerte aus
     */
    @Scheduled(cron = "0 1 0 * * *")
    private void dailyJob(){
        log.info("Daily Job started");

        long startTime = System.currentTimeMillis() / 1000  - (3600 * 24) - 60; // 1 Tag und 60 Sekunden zur Sicherheit
        long endTime = System.currentTimeMillis() / 1000 - 60; // aktuelle Zeit minus 60 Sekunden zur Sicherheit

        List<HourlyData> data = hds.getHourlyDataWithTime(startTime, endTime); // Daten aus der Datenbank holen

        System.out.println("Time: " + startTime + " - " + endTime);
        System.out.println("Data: " + data.size());

        Map<Integer, List<DataWithValue>> dataMap = hds.groupHourlyDataByDevice(data); // Gruppiert die Daten nach Geräten

        addDataToDatabase(dataMap, endTime); // Daten in die Datenbank schreiben

        log.info("Daily Job finished");
    }

    /**
     * Fuehrt die Berechnung der Wochenwerte aus (Montag bis Sonntag) und aktualisiert die Statistik
     */
    @Scheduled(cron = "0 10 0 * * 0")
    private void weeklyJob(){
        log.info("Weekly Job started");

        long startTime = System.currentTimeMillis() / 1000  - (3600 * 24 * 7) - (10 * 60); // 1 Woche und 10 Minuten zur Sicherheit
        long endTime = System.currentTimeMillis() / 1000 - (10 * 60); // aktuelle Zeit minus 10 Minuten zur Sicherheit

        List<DailyData> data = dds.getDailyDataWithTime(startTime, endTime); // Daten aus der Daily Datenbank holen

        System.out.println("Time: " + startTime + " - " + endTime);
        System.out.println("Data: " + data.size());

        Map<Integer, List<DataWithValue>> dataMap = dds.groupDailyDataByDevice(data); // Gruppiert die Daten nach Geräten

        addDataToDatabase(dataMap, endTime); // Daten in die Datenbank schreiben

        log.info("Weekly Job finished");
    }


    private void addDataToDatabase(int gid, List<DataWithValue> deviceData, long time){
        int id = gid;                         // hole die ID des Gerätes aus der Map

        double averageConsumption = statisticService.averageUsage(deviceData); // berechne den Durchschnittsverbrauch
        double idleTime = 0;
        double lowest;
        double peak;
        double total;

        // verschiedene herangehenseweise für berechnung von idle time
        if(deviceData.get(0) instanceof StagingData){ // test welche statistikMethoden angewandt werden müssen
            // hier wird für jedesmal wenn idle = True eine sekunde idletime draufgerechnet
            idleTime = statisticService.idleTime(devicesDataService.getDevicesDataByGid(id).getLowest(), deviceData); // berechne die Leerlaufzeit
            lowest = statisticService.lowest(deviceData);   // berechne den niedrigsten Wert
            peak = statisticService.peak(deviceData);      // berechne den höchsten Wert
            total = statisticService.gesamtUsage(deviceData); // berechne den Gesamtverbrauch
        }
        else {
            ArrayList<TimeData> timeDataArrayList = new ArrayList<TimeData>();
            // umwandeln der DataWithValue in TimeData
            for(DataWithValue element : deviceData){
                timeDataArrayList.add((TimeData) element);
            }
            idleTime = statisticsTimeDataService.idleTime(timeDataArrayList);
            lowest = statisticsTimeDataService.lowest(timeDataArrayList);
            peak = statisticsTimeDataService.peak(timeDataArrayList);
            total = statisticsTimeDataService.gesamtUsage(timeDataArrayList);
        }

        String klasse = deviceData.get(0).getClass().getSimpleName(); // hole den Namen der Klasse aus der ersten Datenzeile

        // je nach Klasse wird die Datenbank ausgewählt
        switch (klasse) {
            case "StagingData" -> hdr.save(new HourlyData(id, time, averageConsumption, total, peak, lowest,idleTime)); // StagingData -> HourlyData
            case "HourlyData" -> ddr.save(new DailyData(id, time, averageConsumption, total, peak, lowest, idleTime));   // HourlyData -> DailyData
            case "DailyData" -> wdr.save(new WeeklyData(id, time, averageConsumption, total, peak, lowest,idleTime));   // DailyData -> WeeklyData
        }

    }


    /**
     * Fuegt die berechneten Daten in die Datenbank ein
     *
     * @param dataMap Daten die in die Datenbank geschrieben werden sollen
     * @param time Zeitpunkt zu dem die Daten berechnet wurden
     */
    @Deprecated
    private void addDataToDatabase(Map<Integer, List<DataWithValue>> dataMap, long time){
        // gehe alle Geräte durch
        for(Map.Entry<Integer, List<DataWithValue>> entry : dataMap.entrySet()){
            int id = entry.getKey();                            // hole die ID des Gerätes aus der Map
            List<DataWithValue> deviceData = entry.getValue();  // hole die Daten des Gerätes aus der Map


            double averageConsumption = statisticService.averageUsage(deviceData); // berechne den Durchschnittsverbrauch
            double idleTime = 0;
            double lowest;
            double peak;
            double total;

            // verschiedene herangehenseweise für berechnung von idle time
            if(deviceData.get(0) instanceof StagingData){ // test welche statistikMethoden angewandt werden müssen
                // hier wird für jedesmal wenn idle = True eine sekunde idletime draufgerechnet
                idleTime = statisticService.idleTime(devicesDataService.getDevicesDataByGid(id).getLowest(), deviceData); // berechne die Leerlaufzeit
                lowest = statisticService.lowest(deviceData);   // berechne den niedrigsten Wert
                peak = statisticService.peak(deviceData);      // berechne den höchsten Wert
                total = statisticService.gesamtUsage(deviceData); // berechne den Gesamtverbrauch
            }
            else {
                ArrayList<TimeData> timeDataArrayList = new ArrayList<TimeData>();
                // umwandeln der DataWithValue in TimeData
                for(DataWithValue element : deviceData){
                    timeDataArrayList.add((TimeData) element);
                }
                idleTime = statisticsTimeDataService.idleTime(timeDataArrayList);
                lowest = statisticsTimeDataService.lowest(timeDataArrayList);
                peak = statisticsTimeDataService.peak(timeDataArrayList);
                total = statisticsTimeDataService.gesamtUsage(timeDataArrayList);
            }

            String klasse = deviceData.get(0).getClass().getSimpleName(); // hole den Namen der Klasse aus der ersten Datenzeile

            // je nach Klasse wird die Datenbank ausgewählt
            switch (klasse) {
                case "StagingData" -> hdr.save(new HourlyData(id, time, averageConsumption, total, peak, lowest,idleTime)); // StagingData -> HourlyData
                case "HourlyData" -> ddr.save(new DailyData(id, time, averageConsumption, total, peak, lowest, idleTime));   // HourlyData -> DailyData
                case "DailyData" -> wdr.save(new WeeklyData(id, time, averageConsumption, total, peak, lowest,idleTime));   // DailyData -> WeeklyData
            }
        }
    }

    public List<DevicesData> getDevices(double lastSeen) {
        return devicesDataService.findDevicesDataByLastSeenAfter(lastSeen);
    }

    /*
     * Hole die Raw Daten für die entsprechende Zeit aus der Datenbank 'staging_data'
     */
    private List<DataWithValue> getRawDataFromDatabase(int gid, double startTime, double endTime) {
        List<StagingData> rawList = sds.findStagingDataByTimeBetweenAndGid(startTime, endTime, gid); // hole die Raw Daten aus der Datenbank

        // gehe alle Raw Daten durch
        // erstelle eine Liste für die Daten
        List<DataWithValue> dataList = new ArrayList<>(rawList);

        // Geht alle Devices der deviceList durch
        return dataList;
    }

}
