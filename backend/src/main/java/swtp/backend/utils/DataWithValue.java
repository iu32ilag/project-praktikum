package swtp.backend.utils;

public abstract class DataWithValue{
    private double value;
    private double idleTime;
    abstract public double getValue();
    abstract public double getIdleTime(); // zur leichten berechung für IdleTime
}
