package swtp.backend.utils.helper;


import org.springframework.stereotype.Service;
import swtp.backend.db.devicesData.DevicesData;
import swtp.backend.db.devicesData.DevicesDataService;
import swtp.backend.utils.statistics.DeviceStatisticService;


@Service
public class CheckDevice {

    DevicesDataService dds;
    DeviceStatisticService dss;

    public void CheckIfNewDevice(int id, double value, double time) {
        DevicesData device = dds.getDevicesDataByGid(id);  // Golte das Gerät aus der DB bei seiner ID

        //Wenn kein Device vorhanden, dann lege es an, ansonsten Updaten.
        if(device == null){
            dds.addDevicesData(id, "", true, time, time, 0, value, value, value,value, value);
        } else {
            int gid = device.getGid();
            String name = device.getCustomID();
            boolean active = value > 0;
            double lowest = device.getLowest() > value && !(value==0)? value : device.getLowest();
            double peak = device.getPeak() > value ? device.getPeak() : value;
            double average = device.getAverage(); //TODO: Berechnung
            double allTime = device.getAllTime() + value;
            Boolean isIdle = dss.checkIdleTime(value,device);
            double idleTime = isIdle ? device.getIdleTime() + 1 : device.getIdleTime();

            dds.updateDevicesData(gid, name, active, time, idleTime, peak, average, lowest, allTime, value, isIdle);
        }

    }
}
