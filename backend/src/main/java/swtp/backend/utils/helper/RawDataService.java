package swtp.backend.utils.helper;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.stereotype.Service;
import swtp.backend.db.devicesData.DevicesData;
import swtp.backend.db.devicesData.DevicesDataService;
import swtp.backend.db.log.stagingData.StagingData;
import swtp.backend.db.log.stagingData.StagingDataRep;
import swtp.backend.utils.statistics.DeviceStatisticService;


/**
 * Klasse zum Parsen von JSON-Strings
 * nimmt die rohdaten von mqtt entgegen und parst diese
 */
@Service
public class RawDataService {
    private final StagingDataRep sdr;
    private final DevicesDataService dds;
    private final DeviceStatisticService dss;


    public RawDataService(StagingDataRep sdr, DevicesDataService dds, DeviceStatisticService deviceStatisticService) {
        this.sdr = sdr;
        this.dds = dds;
        this.dss = deviceStatisticService;
    }
    /**
     * Methode zum Parsen von JSON-Strings
     *
     * @param id  ID des Gerätes
     * @param rawdata Rohdaten als json String
     */
    public void transmitRawData(int id, String rawdata){
        Object obj = JSONValue.parse(rawdata);      // Parst den JSON-String
        JSONObject jsonObject = (JSONObject) obj;   // Castet das Object zu einem JSONObject

        Double time = (Double) jsonObject.get("t");     // Holt den Zeitstempel aus dem JSON-Objekt
        Double value = (Double) jsonObject.get("val");  // Holt den Wert aus dem JSON-Objekt

        if (value >= 0){
            checkForNewDevice(id,value,time);          // Überprüft ob es sich um ein neues Gerät handelt

            try {
                sdr.save(new StagingData(id, time, value)); //Speichert die Daten in der Datenbank
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Methode zum Überprüfen ob ein neues Gerät hinzugefügt werden muss
     *
     * @param id ID des Gerätes
     * @param value Verbrauchswert
     * @param time Zeitstempel
     *
     * TODO: Auslagern in eine eigene Klasse
     */
    private void checkForNewDevice(int id, double value, double time){
        DevicesData device = dds.getDevicesDataByGid(id);  // Golte das Gerät aus der DB bei seiner ID

        //Wenn kein Device vorhanden, dann lege es an, ansonsten Updaten.
        if(device == null){
            dds.addDevicesData(id, "", true, time, time, 0, value, value, value,value, value);
        } else {
            int gid = device.getGid();
            String name = device.getCustomID();
            boolean active = value > 0;
            double lowest = device.getLowest() > value && !(value==0)? value : device.getLowest();
            double peak = device.getPeak() > value ? device.getPeak() : value;
            double average = device.getAverage(); //TODO: Berechnung
            double allTime = device.getAllTime() + value;
            Boolean isIdle = dss.checkIdleTime(value,device);
            double idleTime = isIdle ? device.getIdleTime() + 1 : device.getIdleTime();

            dds.updateDevicesData(gid, name, active, time, idleTime, peak, average, lowest, allTime, value, isIdle);
        }
    }
}
