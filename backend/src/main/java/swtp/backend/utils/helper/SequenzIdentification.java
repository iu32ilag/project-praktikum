package swtp.backend.utils.helper;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import swtp.backend.db.devicesData.DevicesData;
import swtp.backend.db.devicesData.DevicesDataService;
import swtp.backend.db.log.stagingData.StagingData;
import swtp.backend.db.log.stagingData.StagingDataService;
import swtp.backend.db.sequenz.SequenzData;
import swtp.backend.db.sequenz.SequenzDataService;
import swtp.backend.utils.SequenzValue;

import java.util.*;
import java.util.logging.Logger;

@Service
@EnableAsync
public class SequenzIdentification {

    Logger log = Logger.getLogger("Sequenz Identification");

    private final DevicesDataService dds;
    private final StagingDataService sds;
    private final SequenzDataService sqds;

    private final int minLengthInS = 30;    // Min Dauer für Erkennung
    private final double modifikator = 0.4; // Modifikator für Schwellwertberechnung (hier 40%)

    private Map<Integer, List<SequenzValue>> map;

    public SequenzIdentification(DevicesDataService dds, StagingDataService sds, SequenzDataService sqds) {
        this.dds = dds;
        this.sds = sds;
        this.sqds = sqds;
    }

    @Async
    public void start(double startTime, double endTime) {
        // Überprüfe ob bereits eine Map vorhanden ist
        if(map == null) {
            map = new HashMap<>();
        }

        // Hole alle Geräte
        List<DevicesData> devicesData = getDevices(startTime);

        // Überprüfe alle Geräte
        for(DevicesData dd: devicesData) {
            List<StagingData> rawList = getRawDataFromDatabase(dd.getGid(), startTime, endTime); // Hole Rohdaten aus der Datenbank
            rawList.sort(Comparator.comparingDouble(StagingData::getTime));                    // Sortiere Rohdaten nach Zeitstempel
            checkProzess(dd, rawList);                                                       // Führe Überprüfung durch
        }
    }

    /*
    * Hole alle Geräte die seit lastSeen gesehen wurden.
    * Geräte die vorher ihre letzten Daten gesendet haben werden nicht berücksichtigt.
     */
    public List<DevicesData> getDevices(double lastSeen) {
        return dds.findDevicesDataByLastSeenAfter(lastSeen);
    }


    /*
     * Map für Daten
     * Map<Geräte ID: List<Geräte Daten>>
     *  List<Geräte Daten>: speichert die Daten time und value
     *
     * Hole alle Geräte, welche in der letzten Stunde gesehen wurden.
     *
     *
     * Checke ob der übergebene Wert über Schwellwert liegt
     *      -> Wenn nein: beende Proezess
     *      -> Wenn ja: weiter im Prozess
     * Checke ob bereits ein Eintrag für das Gerät vorhanden ist.
     *      -> Wenn nein: Lege Gerät an -> weiter im Prozess
     *      -> Wenn ja: weiter im Prozess
     * Checke ob in der Liste bereits ein Gerät vorhanden ist
     *      -> Wenn nein: füge Gerät einfach in die Liste ein -> beende Prozess
     *      -> Wenn ja: weiter im Prozess
     * Checke ob der vorherige Eintrag 1 Sekunde zurück liegt
     *      -> Wenn ja: füge neuen Eintrag der Liste an -> Weiter im Prozess
     *      -> Wenn nein: leer Liste und füge neuen Eintrag danach ein. -> beende Prozess
     * Checke ob Liste 30 Einträge hat
     *      -> wenn nein: beende Proezess
     *      -> Wenn ja: füge die Daten der Liste in die DB sequenz_data ein und leere Liste
     */
    private void checkProzess(DevicesData dd, List<StagingData> rawList) {
        double schwellwert = dd.getPeak() * modifikator; // Berechne Schwellwert

        // Überprüfe alle Rohdaten
        for (StagingData stagingData : rawList) {
            //liegt Wert nicht über Schwellwert, gehe direkt zu nächsten EIntrag
            if (!(stagingData.getValue() > schwellwert)) {

                //Checke ob vorhandene Einträge über minLenghtInS sind
                if(map.containsKey(stagingData.getGid())) {
                    if(map.get(stagingData.getGid()).size() >= minLengthInS) {
                        //Füge Daten in DB ein
                        writeInDatabase(stagingData.getGid(), map.get(stagingData.getGid()));
                        map.remove(stagingData.getGid());
                    }
                }
                continue;
            }

            // Überprüfe ob kein Eintrag für das Gerät vorhanden ist -> Lege Gerät an
            if (!(map.containsKey(dd.getGid()))) {
                List<SequenzValue> tempList = new ArrayList<>(); // lege liste für die Sequenz an

                SequenzValue sq = new SequenzValue();   // Erstelle ein Sequenzobjekt
                sq.setValue(stagingData.getValue());    // setzte Wert
                sq.setTime(stagingData.getTime());      // setze Zeit

                tempList.add(sq);                       // füge den Sequenzeintrag der Liste hinzu
                map.put(dd.getGid(), tempList);         // Füge die Sequenzliste des Gerätes der Map hinzu
            }else{
                List<SequenzValue> tempList = map.get(dd.getGid()); // hole die Liste des Gerätes aus der Map
                //tempList.sort(Comparator.comparingDouble(SequenzValue::getTime)); // sortiere die Liste nach Zeit

                //Checke ob tempList leer ist
                if(tempList.isEmpty()) {
                    SequenzValue sq = new SequenzValue();   // Erstelle ein Sequenzobjekt
                    sq.setValue(stagingData.getValue());    // setzte Wert
                    sq.setTime(stagingData.getTime());      // setze Zeit

                    tempList.add(sq);                       // füge den Sequenzeintrag der Liste hinzu

                //Checke ob letzter Eintrag 1 Sekunde her ist. Wenn nicht, checke ob Liste 30 Einträge oder mehr hat.
                }else if((int) tempList.get(tempList.size() - 1).getTime() + 1 == (int) stagingData.getTime()) {
                        SequenzValue sq = new SequenzValue();   // Erstelle ein Sequenzobjekt
                        sq.setValue(stagingData.getValue());    // setzte Wert
                        sq.setTime(stagingData.getTime());      // setze Zeit

                        tempList.add(sq);                       // füge den Sequenzeintrag der Liste hinzu
                } else {
                        if (tempList.size() >= minLengthInS) {
                            writeInDatabase(dd.getGid(),tempList); //schreibe Daten in DB
                            map.remove(dd.getGid()); // Lösche Eintrag aus Map

                            SequenzValue sq = new SequenzValue();   // Erstelle ein Sequenzobjekt
                            sq.setValue(stagingData.getValue());    // setzte Wert
                            sq.setTime(stagingData.getTime());      // setze Zeit
                            tempList.add(sq);                       // füge den Sequenzeintrag der Liste hinzu
                        }else {
                            map.remove(dd.getGid()); // Lösche Eintrag aus Map

                            SequenzValue sq = new SequenzValue();   // Erstelle ein Sequenzobjekt
                            sq.setValue(stagingData.getValue());    // setzte Wert
                            sq.setTime(stagingData.getTime());      // setze Zeit
                            tempList.add(sq);                       // füge den Sequenzeintrag der Liste hinzu
                        }
                    }

                map.put(dd.getGid(), tempList);      // Update die Map mit der neuen Liste
            }
        }
    }

    /*
    * Checke ob letzter Wert in liste länger als 1s her ist und ob mindest 30 Einträge vorhanden sind
     */
    private boolean  checkLastValueAndWriteInDB(List<SequenzValue> tempList, DevicesData dd) {
        if (tempList.size() >= minLengthInS) {
            writeInDatabase(dd.getGid(), tempList); //schreibe Daten in DB
            tempList.clear();
            return true;
        }
        return false;
    }


    /*
     * Hole die Raw Daten für die entsprechende Zeit aus der Datenbank 'staging_data'
     */
    private List<StagingData> getRawDataFromDatabase(int gid, double startTime, double endTime) {
        // Geht alle Devices der deviceList durch
        return sds.findStagingDataByTimeBetweenAndGid(startTime, endTime, gid);
    }

    /*
    * Speichere die entsprechende gefundene Sequenz in der Datenbank 'sequenz_data'
    *
    * TODO Überprüfe ob die Sequenz bereits in der Datenbank vorhanden ist
     */
    private void writeInDatabase(int gid, List<SequenzValue> tempList) {
        log.info("Schreibe Sequenz Daten für GID: " + gid + " in DB");
        double startTime = tempList.get(0).getTime();

        for(SequenzValue sv: tempList) {
            SequenzData sd = new SequenzData();
            sd.setGid(gid);
            sd.setTime(sv.getTime());
            sd.setValue(sv.getValue());
            sd.setManual(false);
            sd.setSlabel("Auto Sequenz. Starttime:" + startTime);
            sqds.addSequenzData(sd);
        }
    }
}
