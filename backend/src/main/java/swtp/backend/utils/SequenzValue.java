package swtp.backend.utils;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class SequenzValue {
    private double time;
    private double value;
}
