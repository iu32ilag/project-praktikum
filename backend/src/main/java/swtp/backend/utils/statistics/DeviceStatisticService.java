package swtp.backend.utils.statistics;

import org.springframework.stereotype.Service;
import swtp.backend.db.devicesData.DevicesData;

/**
 * Klasse für Berechnungen der Statistiken der Geräte
 */
@Service
public class DeviceStatisticService {

    /**
     * Überprüft ob der Wert des Gerätes im Bereich der Grenzwerte liegt
     *
     * @param lowest aktuell niedrigster Wert
     * @param value aktueller Wert
     * @return true wenn der Wert im Bereich der Grenzwerte liegt
     */
    public Boolean checkIdleTime(double lowest, double value){
        return value != 0 && value <= (lowest * 1.1); // 10% Toleranz
    }

    /**
     * Überprüft ob der Wert des Gerätes im Bereich der Grenzwerte liegt
     * isIdle wenn value kleiner als
     * niedrigster Wert + 10% der differenz zwischen niedrigstem und höchstem Wert
     *
     * @param value aktueller Wert
     * @param dd Gerätedaten
     * @return true wenn der Wert im Bereich der Grenzwerte liegt
     */
    public Boolean checkIdleTime(double value, DevicesData dd){
        double dif = (dd.getPeak() - dd.getLowest()) * 0.1;
        return value != 0 && value <= (dd.getLowest() + dif); // 10% Toleranz
    }
}
