package swtp.backend.utils.statistics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import swtp.backend.utils.DataWithValue;

import java.util.List;

/**
 * Klasse zur Berechnung von Statistiken
 */
@Service
public class StatisticService {
    DeviceStatisticService dss;


    /**
     * Konstruktor
     *
     * @param dss DeviceStatisticService
     */
    @Autowired
    public StatisticService(DeviceStatisticService dss) {
        this.dss = dss;
    }
    public StatisticService() {
    }


    // List<? extends DataWithValue> wird benötigt,
    // sodass alle KindKlassen von DataWithValue
    // auch dieser Methode gepasst werden können


    /**
     * Berechnet Gesamtsumme der Werte in der Liste
     *
     * @param allData Liste mit allen Daten
     * @return Gesamtsumme der Werte
     */
    // errechnet Gesamtwert, könnte aber bei zu vielen Daten zu einem overflow führen?
    public double gesamtUsage(List<? extends DataWithValue> allData) {
        double gesamt = 0;
        for (DataWithValue element : allData) {
            gesamt += element.getValue();
        }
        return gesamt;
    }

    /**
     * Berechnet den Durchschnittswert der Werte in der Liste
     *
     * @param allData Liste mit allen Daten
     * @return Durchschnittswert der Werte
     */
    // errechnet Durchschnitt über alle Daten, keine unterscheidung nach ID
    public double averageUsage(List<? extends DataWithValue> allData){
        double average = 0;
        int allDataSize = allData.size();
        for (DataWithValue element : allData) {
            average += element.getValue()/allDataSize;
        }
        return average;
    }

    /**
     * BErechnet den hähsten Wert in der Liste
     *
     * @param allData Liste mit allen Daten
     * @return höchster Wert in der Liste
     */
    // errechnet höchsten Verbrauch
    public double peak(List<? extends DataWithValue> allData){
        double peak = 0;
        for (DataWithValue element : allData) {
            peak = Math.max(peak, element.getValue());
        }
        return peak;
    }

    /**
     * Berechnet den niedrigsten Wert in der Liste
     *
     * @param allData Liste mit allen Daten
     * @return  niedrigster Wert in der Liste
     */
    // errechnet niedrigsten Verbrauch
    public double lowest(List<? extends DataWithValue> allData){
        if (allData.isEmpty()){
            return 0;
        }
        double lowest = allData.get(0).getValue();
        for (DataWithValue element : allData) {
            lowest = Math.min(lowest, element.getValue());
        }
        return lowest;
    }

    /**
     * BErechnet die Varianz der Werte in der Liste
     *
     * @param allData Liste mit allen Daten
     * @return Varianz der Werte in der Liste
     */
    public double varianz(List<? extends DataWithValue> allData){
        double averageUsage = averageUsage(allData);
        double varianz = 0;
        for (DataWithValue element : allData) {
            varianz += Math.pow((element.getValue() - averageUsage),2)/ allData.size();
        }
        return varianz;
    }

    /**
     * Berechnet die Idle Zeit der Werte in der Liste
     *
     * @param idleValue Wert, ab dem ein Gerät als idle gilt
     * @param allData   Liste mit allen Daten
     * @return Idle Zeit der Werte in der Liste
     */
    public double idleTime(double idleValue, List<? extends DataWithValue> allData){
        double idleTime = 0;

        for (DataWithValue element : allData) {
            if (dss.checkIdleTime(idleValue, element.getValue())){
                idleTime += 1;
            }
        }
        return idleTime;
    }

    /**
     * Berechnet die Standardabweichung der Werte in der Liste
     *
     * @param allData Liste mit allen Daten
     * @return Standardabweichung der Werte in der Liste
     */
    public double standartabweichung(List<? extends DataWithValue> allData){
        return Math.sqrt(varianz(allData));
    }
}
