package swtp.backend.utils.statistics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import swtp.backend.utils.TimeData;

import java.util.List;

/**
 * Klasse zur Berechnung von Statistiken
 */
@Service
public class StatisticsTimeDataService {
    DeviceStatisticService dss;


    /**
     * Konstruktor
     *
     * @param dss DeviceStatisticService
     */
    @Autowired
    public StatisticsTimeDataService(DeviceStatisticService dss) {
        this.dss = dss;
    }
    public StatisticsTimeDataService() {
    }


    // List<? extends TimeData> wird benötigt,
    // sodass alle KindKlassen von TimeData
    // auch dieser Methode gepasst werden können


    /**
     * Berechnet Gesamtsumme der Werte in der Liste
     *
     * @param allData Liste mit allen Daten
     * @return Gesamtsumme der Werte
     */
    // errechnet Gesamtwert, könnte aber bei zu vielen Daten zu einem overflow führen?
    public double gesamtUsage(List<? extends TimeData> allData) {
        double gesamt = 0;
        for (TimeData element : allData) {
            gesamt += element.getTotal();
        }
        return gesamt;
    }

    /**
     * Berechnet den Durchschnittswert der Werte in der Liste
     *
     * @param allData Liste mit allen Daten
     * @return Durchschnittswert der Werte
     */
    // errechnet Durchschnitt über alle Daten, keine unterscheidung nach ID
    public double averageUsage(List<? extends TimeData> allData){
        double average = 0;
        int allDataSize = allData.size();
        for (TimeData element : allData) {
            average += element.getValue()/allDataSize;
        }
        return average;
    }

    /**
     * BErechnet den hähsten Wert in der Liste
     *
     * @param allData Liste mit allen Daten
     * @return höchster Wert in der Liste
     */
    // errechnet höchsten Verbrauch
    public double peak(List<? extends TimeData> allData){
        double peak = 0;
        for (TimeData element : allData) {
            peak = Math.max(peak, element.getPeak());
        }
        return peak;
    }

    /**
     * Berechnet den niedrigsten Wert in der Liste
     *
     * @param allData Liste mit allen Daten
     * @return  niedrigster Wert in der Liste
     */
    // errechnet niedrigsten Verbrauch
    public double lowest(List<? extends TimeData> allData){
        if (allData.isEmpty()){
            return 0;
        }
        double lowest = allData.get(0).getValue();
        for (TimeData element : allData) {
            lowest = Math.min(lowest, element.getLowest());
        }
        return lowest;
    }

    /**
     * BErechnet die Varianz der Werte in der Liste
     *
     * @param allData Liste mit allen Daten
     * @return Varianz der Werte in der Liste
     */
    public double varianz(List<? extends TimeData> allData){
        double averageUsage = averageUsage(allData);
        double varianz = 0;
        for (TimeData element : allData) {
            varianz += Math.pow((element.getValue() - averageUsage),2)/ allData.size();
        }
        return varianz;
    }

    /**
     * Berechnet die Idle Zeit der Werte in der Liste
     *

     * @param allData   Liste mit allen Daten
     * @return Idle Zeit der Werte in der Liste
     */
    public double idleTime(List<? extends TimeData> allData){
        double idleTime = 0;
        for (TimeData element : allData) {
            idleTime += element.getIdleTime();
        }
        return idleTime;
    }



    /**
     * Berechnet die Standardabweichung der Werte in der Liste
     *
     * @param allData Liste mit allen Daten
     * @return Standardabweichung der Werte in der Liste
     */
    public double standartabweichung(List<? extends TimeData> allData){
        return Math.sqrt(varianz(allData));
    }
}
