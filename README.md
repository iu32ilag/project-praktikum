# Dokumentation
### erreichbarkeit über die VM:
1. Im Uni Wlan sein oder über Cisco mit dem Uni VPN verbinden (Spezial-Alles-2)
2. http://192.168.34.156 aufrufen

### lokale Nutzung:
1. docker desktop installieren
2. git repository clonen `git@git.informatik.uni-leipzig.de:SWS/lehre/ws-2022-2023/swt-p/swtp-2022-09.git`
3. im terminal `docker-compose up -d --remove-orphans` ausführen
    - eventuell im frontend alle instanzen von `192.168.34.156` mit `localhost` austauschen um lokale Daten zu nutzen
4. http://localhost/ aufrufen 
