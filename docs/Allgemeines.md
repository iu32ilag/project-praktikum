# Algemeines

- Spezifikation von ineffizienter Hardware: Bestimmt Kunde anhand der Daten/Statistiken selbst.
-

  

## Teammeetings

- Mittwochs 20 Uhr Discord
- Freitag nach SWT-P Vorlesung Seminarraum S201

## Konsultationen Adesso SE

Uhrzeit: 14:30 - 15:15

Tage:
 - 18.11.2022
 - 09.12.2022  MVP Präsentation in Präsenz bei Adesso SE
 - 20.01.2022
 - 10./17.03.2023
  
  Link: https://conf.fmi.uni-leipzig.de/b/seb-elj-xzz-xch


## "Rollenverteilung"
### Backend
 - David
 - Jiaxuan
 - Marvin
### Frontend
 - Richard 
 - Lena
 - (Ahmad)
### CI/CD Pipeline
 - Arndt
 - Ahmad

## Technologien

### Backend:

- Nutzung von Java + Springboot
- Abrufen der Daten vom MQTT Broker
- Verarbeiten der RAW Daten zu Statistischen Daten
- Abspeichern Statistischer Daten in DB
- Statistische Daten Bereitstellen für Frontend
- Kapselung innerhalb Microserices
- Kommunikation mit Frontend mittels REST-API


### Frontend:
- Nutzung von JS mit VUE.js
- Web-Application
- Analyse Energieverbrauch
- Anzeie Stastischer Werte
- Vergleichbarkeit von Geräten
- Werte siehe [Statistische Auswertung](#statistische-auswertung)
- Gruppierung und Visualisierung von Rechnern anhand von Labels
    - Labelung durch MQTT Topic
    - custom Label hinzufügbar (Location, Typ, Nutzer, ...)
- Kommunikation mit Backend mittels REST-API
  

### Datenbank:

- Datenbank: PostgreSQL
- Speicherplatzbegrenzt für Raw Daten. Begrenzung sollte variabel sein, für spätere skalierbarkeit. 
- Statistische Werte sollen langfristig gespeichert werden.
  


### Server:

- VM mit 30 GB Speicher

  

### Stonstiges

- ggf. Anlegen als Docker?

  

## Daten

Inital als csv Datei. Später dann vom MQTT Broker aller 1 Sekunde von jedem Gerät.
Aktuelle Geräteanzahl 60 Stück. Später erweiterbar.
Geräte sind Labels untergeordnet. 
Möglichkeit Labels hinzuzufügen.

  

### Datenformate:

### Raw Daten:
- MQTT Topic:
- Daten: (Timestamp, Location, Wert)


### Statistische Auswertung:

- Tagesverbrauch / Wochenverbrauch / Monatsverbrauch / Gesamte Zeitraum
- Live Verbrauch
- Idle / Durchschnitts / maximaler Energieverbrauch
