import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import PrimeVue from 'primevue/config';
import Calendar from 'primevue/calendar';
import Card from 'primevue/card';
import StyleClass from 'primevue/styleclass';
import MultiSelect from 'primevue/multiselect';
import Column from 'primevue/column';
import ColumnGroup from 'primevue/columngroup';
import InputText from 'primevue/inputtext';
import Dropdown from 'primevue/dropdown';
import Button from 'primevue/button';
import InputNumber from 'primevue/inputnumber';
import ProgressSpinner from 'primevue/progressspinner';
import SplitButton from 'primevue/splitbutton';
import DataTable from 'primevue/datatable';
import Toast from 'primevue/toast';
import ToastService from 'primevue/toastservice';
import Sidebar from 'primevue/sidebar';

import './assets/styles.scss';

const app = createApp(App);

app.use(router);
app.use(PrimeVue, { ripple: true });
app.use(ToastService);

app.directive('styleclass', StyleClass);
app.component('SplitButton',SplitButton)
app.component('ProgressSpinner',ProgressSpinner)
app.component('InputNumber', InputNumber)
app.component('Button',Button)
app.component('Dropdown', Dropdown);
app.component('InputText', InputText);
app.component('Calendar', Calendar);
app.component('Card', Card);
app.component('Column', Column);
app.component('ColumnGroup', ColumnGroup);
app.component('MultiSelect', MultiSelect);
app.component('DataTable', DataTable);
app.component('Toast', Toast);
app.component('Sidebar', Sidebar);


app.mount('#app')