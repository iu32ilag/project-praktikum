import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Settings.vue'
import Dashboard from '../views/Dashboard.vue'

const router = createRouter({
	history: createWebHistory(),
	routes: [
		{
			path: '/',
			component: Dashboard
		},
		{
			path: '/live',
			component: () => import('../views/Live.vue')
		},
		{
			path: '/sequences',
			component: () => import('../views/Sequences.vue')
		},
		{
			path: '/category',
			component: () => import('../views/Category.vue')
		},{
			path: '/analyse',
			component: () => import('../views/Analyse.vue')
		},{
			path: '/about',
			component: () => import('../views/Settings.vue')
		}
		,{
			path: '/sequenzvergleich',
			component: () => import('../views/Sequenzvergleich.vue')
		}

	]
})

export default router